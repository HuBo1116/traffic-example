package com.grab.statsd;

import com.grab.app.EnrichMap;
import com.grab.app.TrafficUpdate;
import com.grab.kinesis.processor.LocationProcessor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import com.timgroup.statsd.StatsDClient;
import com.timgroup.statsd.NonBlockingStatsDClient;

/**
 * Created by hubo on 16/4/12.
 */
public class DogStatsDWorker implements Runnable{

    private static final String NAME_SPACE = "gostatsd";
    private static final String HOST = TrafficUpdate.appProps.getProperty("application.datadog.host")==null?EnrichMap.appProps.getProperty("application.datadog.host"):TrafficUpdate.appProps.getProperty("application.datadog.host");
    private static final int PORT = TrafficUpdate.appProps.getProperty("application.datadog.port")==null?Integer.parseInt(EnrichMap.appProps.getProperty("application.datadog.port")):Integer.parseInt(TrafficUpdate.appProps.getProperty("application.datadog.port"));

    private static final StatsDClient statsd = new NonBlockingStatsDClient(
            NAME_SPACE,
            HOST,
            PORT
    );
    private static final Log logger = LogFactory.getLog(DogStatsDWorker.class);

    @Override
    public void run() {
        while(true) {
            try{
                if (StatsD.statQueue != null) {
                    Message message = StatsD.statQueue.poll(10, TimeUnit.MILLISECONDS);
                    if (message != null) {
                        this.stat(message);
                    }
                }
            }catch (Exception ex){
                logger.error("error is:", ex);
            }
        }
    }

    private void stat(Message message){
        try {
            switch (message.getType()) {
                case Message.COUNT:
                    statsd.count(message.getKey(), message.getValue(),message.getTags());
                    break;
                default:
                    break;
            }
        }catch (Exception ex) {
            logger.error("update error is:", ex);
        }
    }

}
