package com.grab.statsd;

import com.grab.app.TrafficUpdate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hubo on 16/4/12.
 */
public class StatsD {

    private static final Log Logger = LogFactory.getLog(StatsD.class);
    private static final String LOG_TAG = "statsd";
    private ExecutorService statsdExecutor;

    public static BlockingQueue<Message> statQueue;

    public void initiate(int workerCores) {
        StatsD.statQueue  = new ArrayBlockingQueue<>(1024);
        this.statsdExecutor = Executors.newFixedThreadPool(workerCores);

        for(int i = 0; i < workerCores; ++i) {
            DogStatsDWorker worker = new DogStatsDWorker();
            this.statsdExecutor.execute(worker);
        }
    }


    public static void trackCountWithTags(String contextTag, String method, long value, String[] tags) {
        if (statQueue == null) {
            return;
        }
        String key = contextTag + "." + method + ".value";
        Message message = new Message();
        message.setType(Message.COUNT);
        message.setKey(key);
        message.setValue(value);
        message.setTags(tags);
        statQueue.offer(message);
    }

}
