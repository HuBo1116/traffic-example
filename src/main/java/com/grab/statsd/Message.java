package com.grab.statsd;

/**
 * Created by hubo on 16/4/12.
 */
public class Message {

    public final static String COUNT = "count";
    public final static String GAUGE = "gauge";
    public final static String HISTOGRAM = "histogram";

    private String type;
    private String key;
    private long value;
    private String[] tags;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
