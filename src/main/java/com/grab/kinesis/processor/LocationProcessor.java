package com.grab.kinesis.processor;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.grab.external.controlcenter.VehicleTypeFilter;
import com.grab.kinesis.util.ConfigurationUtils;
import com.grab.kinesis.util.CredentialUtils;
import com.grab.statsd.StatsD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

/**
 * Created by hubo on 16/3/22.
 */
public class LocationProcessor {
    private static final Log Logger = LogFactory.getLog(LocationProcessor.class);
    private static final String CONFIG_FILE_PATH="application.conf";
    public static Properties appProps = new Properties();
    public static StatsD statsD;
    public static Set<Integer> bikeVehicleIDs = new LinkedHashSet<>();

    public static void main(String[] args) throws Exception {
        // load settings file
        loadSettings(CONFIG_FILE_PATH);

        statsD  = new StatsD();
        statsD.initiate(5);

        Region region = RegionUtils.getRegion(appProps.getProperty("application.kinesis.region"));
        if (region == null) {
            System.err.println("ap-southeast-1 is not a valid AWS region.");
            System.exit(1);
        }

        String workerId = String.valueOf(UUID.randomUUID());

        AWSCredentialsProvider credentialsProvider = CredentialUtils.getCredentialsProvider();

        KinesisClientLibConfiguration kclConfig =
                new KinesisClientLibConfiguration(ConfigurationUtils.getClientConfigWithUserAgent().getUserAgent(), appProps.getProperty("application.kinesis.stream.name"), credentialsProvider, workerId)
                        .withRegionName(region.getName())
                        .withCommonClientConfig(ConfigurationUtils.getClientConfigWithUserAgent())
                        .withMaxRecords(Integer.parseInt(appProps.getProperty("applicaiton.kinesis.max.records")))
                        .withIdleTimeBetweenReadsInMillis(Long.parseLong(appProps.getProperty("application.kinesis.read.interval")));


        IRecordProcessorFactory recordProcessorFactory = new LocationTrafficProcessorFactory();

        // Create the KCL worker with the location record processor factory
        Worker worker = new Worker(recordProcessorFactory, kclConfig);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            Logger.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        System.exit(exitCode);
    }

    public static void loadSettings(String configFilePath) {
        if(configFilePath == null)
            configFilePath = "application.conf";
        try {
            FileInputStream in = new FileInputStream(configFilePath);
            appProps.load(in);
            in.close();

            File updatePath = new File(appProps.getProperty("application.update.dir"));
            updatePath.mkdirs();

            File updateAppPath = new File(appProps.getProperty("application.update.app.dir"));
            updateAppPath.mkdirs();

        } catch (IOException e) {
            System.err.println("Unable to load application.conf file.");
            System.exit(1);
        }
    }

    public static void loadBikeTypeFilter(){
        VehicleTypeFilter filter = new VehicleTypeFilter();
        bikeVehicleIDs = filter.getBikeIDs();
    }
}
