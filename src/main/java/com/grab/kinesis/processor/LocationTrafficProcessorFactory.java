package com.grab.kinesis.processor;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;

/**
 * Created by hubo on 16/3/11.
 */
public class LocationTrafficProcessorFactory implements IRecordProcessorFactory {
    /**
     * Constructor.
     */
    public LocationTrafficProcessorFactory() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IRecordProcessor createProcessor() {
        return new LocationTrafficProcessor();
    }
}
