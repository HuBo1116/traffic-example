package com.grab.kinesis.processor;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;

/**
 * Created by hubo on 16/4/22.
 */
public class LocationEnrichProcessorFactory implements IRecordProcessorFactory {
    /**
     * Constructor.
     */
    public LocationEnrichProcessorFactory() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IRecordProcessor createProcessor() {
        return new LocationEnrichProcessor();
    }
}
