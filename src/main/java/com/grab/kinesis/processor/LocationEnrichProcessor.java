package com.grab.kinesis.processor;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;
import com.google.protobuf.InvalidProtocolBufferException;
import com.grab.app.EnrichMap;
import com.grab.app.TrafficUpdate;
import com.grab.engine.enrich.EnrichEngineExecuter;
import com.grab.kinesis.model.Location;
import com.grab.kinesis.pb.DriverLocationProtos;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hubo on 16/4/22.
 */
public class LocationEnrichProcessor implements IRecordProcessor {
    private static final Log Logger = LogFactory.getLog(LocationEnrichProcessor.class);
    private String kinesisShardId;
    private static long VALID_WINDOW = 15 * 60 * 1000; //within 15 min is valid
    private static final String[] RECEIVED_BYTES_TAGS = {"stream:"+ TrafficUpdate.appProps.getProperty("application.kinesis.stream.name")};
    private static final String[] RECEIVED_TAGS = {"stream:drvierlocation"};

    @Override
    public void initialize(String shardId) {
        Logger.info("Initializing record processor for shard: " + shardId);
        this.kinesisShardId = shardId;
    }

    @Override
    public void processRecords(List<Record> records, IRecordProcessorCheckpointer checkpointer) {
        EnrichEngineExecuter.processRecordExecutor.submit(() -> processRecordV2(records));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdown(IRecordProcessorCheckpointer checkpointer, ShutdownReason reason) {
        Logger.info("Shutting down record processor for shard: " + kinesisShardId);
    }

    private void updateEnrichMap(Location location){
        EnrichMap.locationQueue.offer(location);
    }

    private void processRecordV2(List<Record> records) {
        for (Record record : records) {
//            statReceivedSize(record.getData().array().length);
            if (System.currentTimeMillis() - record.getApproximateArrivalTimestamp().getTime() > VALID_WINDOW) {
                continue;
            }

            List<Location> list = getLocationByProto(record);
            for(Location location: list) {
                updateEnrichMap(location);
            }
        }
    }

    private List<Location> getLocationByProto(Record record){
        List<Location> locationList = new ArrayList<>();
        try {
            DriverLocationProtos.DriverLocationBag locationBags = DriverLocationProtos.DriverLocationBag.parseFrom(record.getData().array());
//            statReceived(locationBags.getEntriesList().size());
            for (DriverLocationProtos.DriverLocation locationProto : locationBags.getEntriesList()) {
                Location location = new Location();
                location.setDriverID(locationProto.getDriverID());
                location.setCityID(locationProto.getCityID());
                int[] taxiTypeIds = new int[locationProto.getVehicleTypeIDsList().size()];
                for(int i=0;i<locationProto.getVehicleTypeIDsList().size();i++){
                    taxiTypeIds[i] = (int)locationProto.getVehicleTypeIDsList().get(i).longValue();
                }
                if (taxiTypeIds.length > 0) {
                    location.setTaxiTypeId((long)taxiTypeIds[0]);
                }
                location.setTaxiTypeIDs(taxiTypeIds);
                location.setLat(locationProto.getLatitude());
                location.setLng(locationProto.getLongitude());
                location.setAccuracy(locationProto.getAccuracy()+"");
                location.setAltitude(locationProto.getAltitude()+"");
                location.setBearing(locationProto.getBearing()+"");
                location.setSource(locationProto.getSource());
                location.setSpeed(locationProto.getSpeed());
                location.setTs(locationProto.getTimestamp());
                locationList.add(location);
            }
        } catch (InvalidProtocolBufferException e) {
            Logger.warn("Skipping record. Unable to parse record into Location. Partition Key: " + record.getPartitionKey());
        }
        return locationList;
    }

//    private void statReceivedSize(int value){
//        StatsD.trackCountWithTags("sk2-consumers", "bytes-received", value, RECEIVED_BYTES_TAGS);
//    }

//    private void statReceived(int value){
//        StatsD.trackCountWithTags("streams","received",value,RECEIVED_TAGS);
//    }

}
