package com.grab.kinesis.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by hubo on 16/3/11.
 */
public class Location {
    private final static ObjectMapper JSON = new ObjectMapper();
    static {
        JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private String driverID;
    private String cityID;
    private int[] taxiTypeIDs;
    private int vehicleTypeID;
    private double lat;
    private double lng;
    private String accuracy;
    private String altitude;
    private String bearing;
    private String source;
    private double speed;
    private long ts;
    private long taxiTypeId;
    private int vehicleType;

    public Location() {
    }

    public Location(String id, String cityCode, double lat, double lon, double speed, long timestamp) {
        this.driverID = id;
        this.cityID = cityCode;
        this.lat = lat;
        this.lng = lon;
        this.speed = speed;
        this.ts = timestamp;
    }



    public byte[] toJsonAsBytes() {
        try {
            return JSON.writeValueAsBytes(this);

        } catch (IOException e) {
            return null;
        }
    }



    public static Location fromJsonAsBytes(byte[] bytes) {
        try {
            return JSON.readValue(bytes, Location.class);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return String.format("ID:%s,CITYCODE:%s,LAT:%.4f,L0N:%.4f,SPEED:%.2f,vehicleTypeID:%d,taxiTypeIDs:%d,ts:%d",
                driverID, cityID, lat, lng, speed,vehicleTypeID,taxiTypeIDs[0],ts);
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public int[] getTaxiTypeIDs() {
        return taxiTypeIDs;
    }

    public void setTaxiTypeIDs(int[] taxiTypeIDs) {
        this.taxiTypeIDs = taxiTypeIDs;
    }

    public int getVehicleTypeID() {
        return vehicleTypeID;
    }

    public void setVehicleTypeID(int vehicleTypeID) {
        this.vehicleTypeID = vehicleTypeID;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getBearing() {
        return bearing;
    }

    public void setBearing(String bearing) {
        this.bearing = bearing;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public long getTaxiTypeId() {
        return taxiTypeId;
    }

    public void setTaxiTypeId(long taxiTypeId) {
        this.taxiTypeId = taxiTypeId;
    }

    public int getType() {
        return vehicleType;
    }

    public void setType(int type) {
        this.vehicleType = type;
    }
}
