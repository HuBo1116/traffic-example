package com.grab.kinesis.util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;

/**
 * Created by hubo on 16/3/11.
 */
public class CredentialUtils {
    public static AWSCredentialsProvider getCredentialsProvider() throws Exception {
        /*
         * The ProfileCredentialsProvider will return your [default] credential profile by
         * reading from the credentials file located at (~/.aws/credentials).
         */
        AWSCredentialsProvider credentialsProvider = null;
        try {
            credentialsProvider = new PropertiesFileCredentialsProvider("application.conf");
        } catch (Exception e) {
            throw new AmazonClientException("Cannot load the default config file in project: application.conf", e);
        }
        return credentialsProvider;
    }
}
