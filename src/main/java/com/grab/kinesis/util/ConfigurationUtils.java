package com.grab.kinesis.util;

import com.amazonaws.ClientConfiguration;
import com.grab.app.TrafficUpdate;

/**
 * Created by hubo on 16/3/11.
 */
public class ConfigurationUtils {
    public static ClientConfiguration getClientConfigWithUserAgent() {
        final ClientConfiguration config = new ClientConfiguration();
        final StringBuilder userAgent = new StringBuilder();

        userAgent.append(TrafficUpdate.appProps.getProperty("application.kinesis.application.name"));
        config.setUserAgent(userAgent.toString());
        return config;
    }
}
