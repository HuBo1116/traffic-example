package com.grab.engine.common;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by hubo on 16/4/22.
 */
public class Constant {
    public static final double MALAYSIA_SINGAPORE_MIN_LON = 99.641277;
    public static final double MALAYSIA_SINGAPORE_MAX_LON = 120;
    public static final double MALAYSIA_SINGAPORE_MIN_LAT = 0.85;
    public static final double MALAYSIA_SINGAPORE_MAX_LAT = 7.383333;

    public static final double VIETNAM_MIN_LON = 102.216667;
    public static final double VIETNAM_MAX_LON = 109.466667;
    public static final double VIETNAM_MIN_LAT = 8.383333;
    public static final double VIETNAM_MAX_LAT = 23.666667;

    public static final double THAILAND_MIN_LON = 97.366667;
    public static final double THAILAND_MAX_LON = 105.766667;
    public static final double THAILAND_MIN_LAT = 5.616667;
    public static final double THAILAND_MAX_LAT = 20.442778;

    public static final double PHILIPPINES_MIN_LON = 116.65;
    public static final double PHILIPPINES_MAX_LON = 126.604444;
    public static final double PHILIPPINES_MIN_LAT = 4.588889;
    public static final double PHILIPPINES_MAX_LAT = 21.113056;

    public static final double INDONESIA_MIN_LON = 	94.970278;
    public static final double INDONESIA_MAX_LON = 141.016667;
    public static final double INDONESIA_MIN_LAT = -11;
    public static final double INDONESIA_MAX_LAT = 10.616667;

    public static final Set<String> MALAYSIA_CITY_CODE;
    public static final Set<String> SINGAPORE_CITY_CODE;
    public static final Set<String> VIETNAM_CITY_CODE;
    public static final Set<String> THAILAND_CITY_CODE;
    public static final Set<String> PHILIPPINES_CITY_CODE;
    public static final Set<String> INDONESIA_CITY_CODE;

    static {
        Set<String> malaysiaSet = new HashSet<>();
        malaysiaSet.add("KUL");
        malaysiaSet.add("JHB");
        malaysiaSet.add("MKZ");
        malaysiaSet.add("KCH");
        malaysiaSet.add("PEN");
        malaysiaSet.add("KUT");
        malaysiaSet.add("BKI");
        MALAYSIA_CITY_CODE = Collections.unmodifiableSet(malaysiaSet);
    }
    static {
        Set<String> philippinesSet = new HashSet<>();
        philippinesSet.add("MNL");
        philippinesSet.add("CEB");
        philippinesSet.add("DVO");
        philippinesSet.add("ILO");
        philippinesSet.add("BAC");
        philippinesSet.add("BAG");
        philippinesSet.add("CDO");
        PHILIPPINES_CITY_CODE = Collections.unmodifiableSet(philippinesSet);
    }

    static {
        Set<String> thailandSet = new HashSet<>();
        thailandSet.add("BKK");
        thailandSet.add("PYX");
        thailandSet.add("HKT");
        thailandSet.add("CNX");
        thailandSet.add("CEI");
        THAILAND_CITY_CODE = Collections.unmodifiableSet(thailandSet);
    }

    static {
        Set<String> sinSet = new HashSet<>();
        sinSet.add("SIN");
        SINGAPORE_CITY_CODE = Collections.unmodifiableSet(sinSet);
    }

    static {
        Set<String> vietnamSet = new HashSet<>();
        vietnamSet.add("SGN");
        vietnamSet.add("HNI");
        vietnamSet.add("DAN");
        VIETNAM_CITY_CODE = Collections.unmodifiableSet(vietnamSet);
    }

    static {
        Set<String> indonesiaSet = new HashSet<>();
        indonesiaSet.add("CGK");
        indonesiaSet.add("SOL");
        indonesiaSet.add("SUB");
        indonesiaSet.add("PDG");
        indonesiaSet.add("BAL");
        indonesiaSet.add("BDO");
        INDONESIA_CITY_CODE = Collections.unmodifiableSet(indonesiaSet);
    }


}
