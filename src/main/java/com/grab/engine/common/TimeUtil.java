package com.grab.engine.common;

import java.util.Calendar;

/**
 * Created by hubo on 16/6/1.
 */
public class TimeUtil {

    public static int getHourOfWeek(long timeMicros) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeMicros/1000);

        int dowVal = c.get(Calendar.DAY_OF_WEEK);
        int dow=0;
        switch(dowVal){
            case Calendar.SUNDAY: dow=0; break;
            case Calendar.MONDAY: dow=1; break;
            case Calendar.TUESDAY: dow=2; break;
            case Calendar.WEDNESDAY: dow=3; break;
            case Calendar.THURSDAY: dow=4; break;
            case Calendar.FRIDAY: dow=5; break;
            case Calendar.SATURDAY: dow=6; break;
        }

        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);

        return dow*24+hourOfDay;
    }
}
