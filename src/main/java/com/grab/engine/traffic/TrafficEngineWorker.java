package com.grab.engine.traffic;


import com.grab.app.TrafficUpdate;
import com.grab.engine.geom.GPSPoint;
import com.grab.engine.geom.SpeedSample;
import com.grab.kinesis.model.Location;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by hubo on 16/3/22.
 */
public class TrafficEngineWorker implements Runnable {
    private static final Log logger = LogFactory.getLog(TrafficEngineWorker.class);
    private BlockingQueue<Location> locationQueue;

    public TrafficEngineWorker(BlockingQueue<Location> locationQueue){
        this.locationQueue = locationQueue;
    }

    @Override
    public void run() {
        while(true) {
            try{
                Location location = this.locationQueue.poll(10, TimeUnit.MILLISECONDS);
                if (location != null) {
                    this.updateTraffic(location);
                }
            }catch (Exception ex){
                logger.error("worker failed caused by:", ex);
            }
        }
    }

    private void updateTraffic(Location location) {
        try {
            GPSPoint pt = new GPSPoint(location);
            List<SpeedSample> speeds = TrafficUpdate.trafficEngine.update(pt);
            if(speeds != null && speeds.size() != 0){
                TrafficUpdate.trafficCondition.ingest(speeds, location.getType());
            }
        }catch (Exception ex) {
            logger.error("update traffic failed caused by:", ex);
        }
    }
}
