package com.grab.engine.traffic;

import com.conveyal.osmlib.OSM;
import com.conveyal.osmlib.Way;
import com.grab.engine.geom.*;
import com.grab.engine.osm.OSMUtils;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.index.quadtree.Quadtree;
import com.vividsolutions.jts.linearref.LengthIndexedLine;
import java.util.*;

/**
 * Created by hubo on 16/4/19.
 */
public class TrafficEngine {
    // The distance from an intersection, measured along a road, where a tripline crosses.
    private static final double INTERSECTION_MARGIN_METERS = 10;
    // Max car speed. Anything faster is considered noise.
    private static final double MAX_SPEED = 31.0;
    // Max time between two successive GPS fixes from a single vehicle. Anything longer is considered noise.
    private static final int MAX_GPS_PAIR_DURATION = 20;
    // Minimum distance of a way tracked by the traffic engine. Must be longer than twice the intersection margin, or else
    // triplines would be placed out of order.
    private static final double MIN_SEGMENT_LEN = INTERSECTION_MARGIN_METERS*2;

    //====STREET DATA=====
    // All triplines
    List<TripLine> triplines = new ArrayList<>();
    // Way id -> node indexes corresponding to tripline clusters. A tripline cluster is one or two triplines
    // flanking an intersection.
    Map<Long, List<Integer>> clusters = new HashMap<>();
    // Spatial index stuff
    Envelope engineEnvelope = new Envelope();
    // for traffic tripline
    private Quadtree index = new Quadtree();

    //====VEHICLE STATE=====
    // Vehicle id -> last encountered GPS fix
    Map<String, GPSPoint> lastPoint = new HashMap<>();
    // Vehicle id -> all pending crossings
    Map<String, Set<Crossing>> pendingCrossings = new HashMap<>();
    // (tripline1, tripline2) -> count of dropoff lines.
    Map<TripLine, Map<TripLine,Integer>> dropOffs = new HashMap<>();

    //====STATISTICS====
    // A count of trip events per tripline.
    Map<TripLine, Integer> tripEvents = new HashMap<>();

    public TrafficEngine(){
    }

    public void setStreets(OSM osm) {
        addTripLines(osm);
    }

    private void addTripLines(OSM osm) {
        // find intersection nodes
        Set<Long> intersections = OSMUtils.findIntersections(osm);

        // for each way
        for (Map.Entry<Long, Way> wayEntry : osm.ways.entrySet()) {
            long wayId = wayEntry.getKey();
            Way way = wayEntry.getValue();

            if (!OSMUtils.checkIsAcceptableHighway(way)){
                continue;
            }

            // Get the way's geometry
            LineString wayPath;
            try {
                wayPath = OSMUtils.getLineStringForWay(way, osm);
            } catch (RuntimeException ex) {
                continue;
            }

            // Check that it's long enough
            double wayLen = OSMUtils.getLength(wayPath); // meters
            if(wayLen < MIN_SEGMENT_LEN){
                continue;
            }

            LengthIndexedLine indexedWayPath = new LengthIndexedLine(wayPath);
            double startIndex = indexedWayPath.getStartIndex();
            double endIndex = indexedWayPath.getEndIndex();

            // find topological units per meter
            double scale = (endIndex - startIndex) / wayLen; // topos/meter

            // meters * topos/meter = topos
            double intersection_margin = INTERSECTION_MARGIN_METERS * scale;

            int tlIndex = 0;
            int tlClusterIndex = 0;
            double lastDist = 0;
            // for each node in the way
            for (int i = 0; i < way.nodes.length; i++) {
                Long nd = way.nodes[i];

                // only place triplines at ends and intersections
                if( !(i == 0 || i == way.nodes.length - 1 || intersections.contains(nd)) ){
                    continue;
                }

                // get the linear reference of this nd along the way
                Point pt = wayPath.getPointN(i);
                double ptIndex = indexedWayPath.project(pt.getCoordinate());
                double ptDist = ptIndex/scale;

                // ensure the distance since the last tripline cluster is long enough
                // or else triplines will be out of order
                if(ptDist-lastDist < MIN_SEGMENT_LEN){
                    continue;
                }
                lastDist = ptDist;

                // log the cluster index so we can slice up the OSM later
                List<Integer> wayClusters = clusters.get(wayId);
                if( wayClusters == null ){
                    wayClusters = new ArrayList<>();
                    clusters.put( wayId, wayClusters );
                }
                wayClusters.add( i );

                engineEnvelope.expandToInclude(pt.getCoordinate());

                boolean oneway = isOneway(way);

                tlIndex = createTrafficTripLine(wayId, indexedWayPath, startIndex, endIndex, scale, intersection_margin, tlIndex, tlClusterIndex, i, nd, ptIndex, oneway);

                tlClusterIndex += 1;
            }
        }
    }


    /**
     * Update the traffic engine with a new GPS fix. If the GPS fix trips a tripline
     * which completes a pending crossing, a speed sample will be returned. A single GPS
     * fix can result in more than one speed samples.
     * @param gpsPoint
     * @return
     */
    public List<SpeedSample> update(GPSPoint gpsPoint) {
        GPSSegment gpsSegment = checkIsValid(gpsPoint);

        if (gpsSegment == null) return null;


        List<Crossing> segCrossings = getCrossingsInOrder(gpsSegment);

        List<SpeedSample> ret = new ArrayList<>();
        for (Crossing crossing : segCrossings) {
            recordCrossingCount(crossing.tripline);

            Crossing lastCrossing = getLastCrossingAndUpdatePendingCrossings(gpsPoint.getVehicleId(), crossing);

            SpeedSample ss = getAdmissableSpeedSample(lastCrossing, crossing);
            if(ss==null){
                continue;
            }

            ret.add( ss );
        }
        return ret;
    }

    private GPSSegment checkIsValid(GPSPoint gpsPoint) {
        // update the state for the GPSPoint's vehicle
        GPSPoint p0 = lastPoint.get(gpsPoint.getVehicleId());
        lastPoint.put(gpsPoint.getVehicleId(), gpsPoint);

        // null if this is the vehicle's first point
        if (p0 == null) {
            return null;
        }

        // If the time elapsed since this vehicle's last point is
        // larger than MAX_GPS_PAIR_DURATION, the line that connects
        // them may not be colinear to a street; it's thrown out as
        // not useful.
        if( gpsPoint.getTime() - p0.getTime() > MAX_GPS_PAIR_DURATION * 1000000 ) {
            return null;
        }

        GPSSegment gpsSegment = new GPSSegment(p0, gpsPoint);

        // if the segment is sitting still, it can't cross a tripline
        if (gpsSegment.isStill()) {
            return null;
        }
        return gpsSegment;
    }

    private SpeedSample getAdmissableSpeedSample(Crossing lastCrossing, Crossing crossing) {
        if(lastCrossing == null){
            return null;
        }

        // don't record speeds for vehicles heading up the road in the wrong direction, if it's a one-way road
        if(crossing.tripline.ndIndex < lastCrossing.tripline.ndIndex && crossing.tripline.oneway){
            return null;
        }

        // it may be useful to keep the displacement sign, but the order of the
        // ndIndex associated with each tripline gives the direction anyway
        double ds = Math.abs(crossing.tripline.dist - lastCrossing.tripline.dist); // meters
        double dt = crossing.getTime() - lastCrossing.getTime(); // seconds

        if( dt < 0 ){
            return null;
//			throw new RuntimeException( String.format("this crossing happened before %fs before the last crossing", dt) );
        }

        if( dt==0 ){
            return null;
        }

        double speed = ds / dt; // meters per second

        if( speed > MAX_SPEED ){
            return null; // any speed sample above MAX_SPEED is assumed to be GPS junk.
        }

        SpeedSample ss = new SpeedSample(lastCrossing, crossing, speed);

        return ss;
    }

    private Crossing getLastCrossingAndUpdatePendingCrossings(String vehicleId, Crossing crossing) {
        // get pending crossings for this vehicle
        Set<Crossing> vehiclePendingCrossings = pendingCrossings.get(vehicleId);
        if(vehiclePendingCrossings == null){
            vehiclePendingCrossings = new HashSet<Crossing>();
            pendingCrossings.put( vehicleId, vehiclePendingCrossings );
        }

        // see if this crossing completes any of the pending crossings
        Crossing lastCrossing = null;
        for( Crossing vehiclePendingCrossing : vehiclePendingCrossings ){
            if( vehiclePendingCrossing.completedBy( crossing ) ){
                lastCrossing = vehiclePendingCrossing;

                // when a pending crossing is completed, a bunch of pending crossing are left
                // that will never be completed. These pending crossings are "drop-off points",
                // where a GPS trace tripped a line but somehow dropped off the line segment between
                // a pair of trippoints, never to complete it. We can record the tripline that
                // _started_ the pair that was eventually completed as the place where the drop-off
                // was picked back up. By doing this we can identify OSM locations with poor connectivity

                TripLine pickUp = lastCrossing.getTripline();
                for( Crossing dropOffCrossing : vehiclePendingCrossings ){
                    if( lastCrossing.equals( pickUp ) ){
                        continue;
                    }

                    TripLine dropOff = dropOffCrossing.getTripline();

                    //if( pickUp.wayId==dropOff.wayId && pickUp.tlClusterIndex==dropOff.tlClusterIndex ){
                    if( pickUp.wayId==dropOff.wayId ){
                        continue;
                    }

                    Map<TripLine,Integer> pickups = dropOffs.get( dropOff );
                    if(pickups==null){
                        pickups = new HashMap<TripLine,Integer>();
                        dropOffs.put( dropOff, pickups );
                    }
                    Integer pickupCount = pickups.get( pickUp );
                    if(pickupCount==null){
                        pickupCount = 0;
                    }
                    pickups.put(pickUp, pickupCount+1);

                }

                // if this crossing completes a pending crossing, then this crossing
                // wins and all other pending crossings are deleted
                vehiclePendingCrossings = new HashSet<Crossing>();
                pendingCrossings.put( vehicleId, vehiclePendingCrossings );

                break;
            }
        }

        // this crossing is now a pending crossing
        vehiclePendingCrossings.add( crossing );
        return lastCrossing;
    }

    private void recordCrossingCount(TripLine tripline) {
        // record a crossing count for each tripline. Comes in handy, especially for
        // dropoff analysis
        if( !tripEvents.containsKey( tripline ) ){
            tripEvents.put( tripline, 0 );
        }
        tripEvents.put( tripline, tripEvents.get(tripline)+1 );
    }

    private List<Crossing> getCrossingsInOrder(GPSSegment gpsSegment) {

        List<Crossing> ret = new ArrayList<>();

        List<?> tripLines = index.query(gpsSegment.getEnvelope());
        for (Object tlObj : tripLines) {
            TripLine tl = (TripLine) tlObj;

            Crossing crossing = gpsSegment.getCrossing(tl);

            if (crossing != null) {
                ret.add( crossing );
            }
        }

        Collections.sort(ret, (o1, o2) -> {
            if( o1.timeMicros < o2.timeMicros ){
                return -1;
            }
            if( o1.timeMicros > o2.timeMicros ){
                return 1;
            }
            return 0;
        });

        return ret;
    }

    private boolean isOneway(Way way) {
        return way.tagIsTrue("oneway") ||
                (way.hasTag("highway") && way.getTag("highway").equals("motorway")) ||
                (way.hasTag("junction") && way.getTag("junction").equals("roundabout"));
    }

    private int createTrafficTripLine(long wayId, LengthIndexedLine indexedWayPath, double startIndex, double endIndex, double scale, double intersection_margin, int tlIndex, int tlClusterIndex, int i, Long nd, double ptIndex, boolean oneway) {
        // create the tripline preceding the intersection
        double preIndex = ptIndex - intersection_margin;

        if (preIndex >= startIndex) {
            TripLine tl = OSMUtils.genTripline(wayId, i, nd, tlIndex, tlClusterIndex, indexedWayPath, scale, preIndex, oneway);
            index.insert(tl.getEnvelope(), tl);
            triplines.add(tl);
            tlIndex += 1;
        }

        // create the tripline following the intersection
        double postIndex = ptIndex + intersection_margin;
        if (postIndex <= endIndex) {
            TripLine tl = OSMUtils.genTripline(wayId, i, nd, tlIndex, tlClusterIndex, indexedWayPath, scale, postIndex, oneway);
            index.insert(tl.getEnvelope(), tl);
            triplines.add(tl);
            tlIndex += 1;
        }
        return tlIndex;
    }
}
