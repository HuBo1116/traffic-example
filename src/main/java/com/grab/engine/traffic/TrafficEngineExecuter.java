package com.grab.engine.traffic;

import com.grab.kinesis.model.Location;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hubo on 16/3/22.
 */
public class TrafficEngineExecuter {
    private static final Log Logger = LogFactory.getLog(TrafficEngineExecuter.class);
    private ExecutorService updateTrafficExecutor;
    public static ExecutorService processRecordExecutor;
    private static int shardNum = 10;

    public void start(int workerCores,BlockingQueue<Location> locationQueue) {
        this.updateTrafficExecutor = Executors.newFixedThreadPool(workerCores);
        this.processRecordExecutor = Executors.newFixedThreadPool(shardNum);

        for(int i = 0; i < workerCores; i++) {
            TrafficEngineWorker worker = new TrafficEngineWorker(locationQueue);
            this.updateTrafficExecutor.execute(worker);
        }
    }
}
