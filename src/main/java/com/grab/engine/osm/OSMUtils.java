package com.grab.engine.osm;

import com.conveyal.osmlib.Node;
import com.conveyal.osmlib.OSM;
import com.conveyal.osmlib.Way;
import com.grab.engine.geom.StreetSegment;
import com.grab.engine.geom.TripLine;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.linearref.LengthIndexedLine;
import org.geotools.data.DataUtilities;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.data.store.ContentFeatureSource;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by hubo on 16/4/19.
 */
public class OSMUtils {

    private static GeodeticCalculator gc = new GeodeticCalculator();
    // The distance of a tripline to one side of the street. The width of the tripline is twice the radius.
    private static final double TRIPLINE_RADIUS = 15;

    static public LineString getLineStringForWay(Way way, OSM osm) {
        Coordinate[] coords = new Coordinate[way.nodes.length];
        for (int i = 0; i < coords.length; i++) {
            Long nd = way.nodes[i];
            Node node = osm.nodes.get(nd);

            if (node == null) {
                throw new RuntimeException("Way contains unknown node " + nd);
            }

            coords[i] = new Coordinate(node.getLon(), node.getLat());
        }

        return new GeometryFactory().createLineString(coords);
    }

    /**
     * Returns the id of every node encountered in an OSM dataset more than once.
     * @param osm
     * @return
     */
    static public Set<Long> findIntersections(OSM osm) {
        Set<Long> ret = new HashSet<>();

        // link nodes to the ways that visit them
        Map<Long, Integer> ndToWay = new HashMap<>();
        for (Map.Entry<Long, Way> wayEntry : osm.ways.entrySet()) {
            Way way = wayEntry.getValue();

            for (Long nd : way.nodes) {
                Integer count = ndToWay.get(nd);
                if (count == null) {
                    ndToWay.put(nd, 1);
                } else {
                    ret.add(nd);
                }
            }
        }

        return ret;
    }

    static public boolean checkIsAcceptableHighway(Way way) {
        String highwayType = way.getTag("highway");
        if(highwayType == null){
            return false;
        }

        // Check to make sure it's an acceptable type of highway
        String[] motorwayTypes = {"motorway","trunk",
                "primary","secondary","tertiary","unclassified",
                "residential","service","motorway_link","trunk_link",
                "primary_link","secondary_link","tertiary_link"};
        if(!among(highwayType,motorwayTypes)){
            return false;
        }
        return true;
    }

    static private boolean among(String str, String[] ary) {
        for(String item : ary){
            if(str.equals(item)){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the length in meters of a line expressed in lat/lng pairs.
     * @param wayPath
     * @return
     */
    static public double getLength(LineString wayPath) {
        double ret = 0;
        for (int i = 0; i < wayPath.getNumPoints() - 1; i++) {
            Point p1 = wayPath.getPointN(i);
            Point p2 = wayPath.getPointN(i + 1);
            double dist = getDistance(p1.getX(), p1.getY(), p2.getX(), p2.getY());
            ret += dist;
        }
        return ret;
    }

    /**
     * Distance between two points.
     * @param lon1
     * @param lat1
     * @param lon2
     * @param lat2
     * @return
     */
    static private double getDistance(double lon1, double lat1, double lon2, double lat2) {
        gc.setStartingGeographicPoint(lon1, lat1);
        gc.setDestinationGeographicPoint(lon2, lat2);
        return gc.getOrthodromicDistance();
    }

    static public TripLine genTripline(long wayId, int ndIndex, long ndID, int tlIndex, int tlClusterIndex, LengthIndexedLine lil, double scale,
                                        double lengthIndex, boolean oneway) {
        double l1Bearing = getBearing(lil, lengthIndex);

        Coordinate p1 = lil.extractPoint(lengthIndex);
        gc.setStartingGeographicPoint(p1.x, p1.y);
        gc.setDirection(clampAzimuth(l1Bearing + 90), TRIPLINE_RADIUS);
        Point2D tlRight = gc.getDestinationGeographicPoint();
        gc.setDirection(clampAzimuth(l1Bearing - 90), TRIPLINE_RADIUS);
        Point2D tlLeft = gc.getDestinationGeographicPoint();

        TripLine tl = new TripLine(tlRight, tlLeft, wayId, ndIndex, ndID, tlIndex, tlClusterIndex, lengthIndex / scale, oneway);
        return tl;
    }

    /**
     * Find the tangential to a point on a linestring.
     * @param lil length indexed line
     * @param index index
     * @return
     */
    static public double getBearing(LengthIndexedLine lil, double index) {
        double epsilon = 0.000009;
        double i0, i1;

        if (index - epsilon <= lil.getStartIndex()) {
            i0 = lil.getStartIndex();
            i1 = i0 + epsilon;
        } else if (index + epsilon >= lil.getEndIndex()) {
            i1 = lil.getEndIndex();
            i0 = i1 - epsilon;
        } else {
            i0 = index - (epsilon / 2);
            i1 = index + (epsilon / 2);
        }

        Coordinate p1 = lil.extractPoint(i0);
        Coordinate p2 = lil.extractPoint(i1);

        gc.setStartingGeographicPoint(p1.x, p1.y);
        gc.setDestinationGeographicPoint(p2.x, p2.y);
        return gc.getAzimuth();
    }


    /**
     * Clamps all angles to the azimuth range -180 degrees to 180 degrees.
     * @param d
     * @return
     */
    static public double clampAzimuth(double d) {
        d %= 360;

        if (d > 180.0) {
            d -= 360;
        } else if (d < -180) {
            d += 360;
        }

        return d;
    }

    static public TripLine genProbeTripline(long wayId, LengthIndexedLine lil, double lengthIndex, GeometryFactory gf) {
        double l1Bearing = getBearing(lil, lengthIndex);

        Coordinate p1 = lil.extractPoint(lengthIndex);
        gc.setStartingGeographicPoint(p1.x, p1.y);
        gc.setDirection(clampAzimuth(l1Bearing + 90), TRIPLINE_RADIUS);
        Point2D tlRight = gc.getDestinationGeographicPoint();
        gc.setDirection(clampAzimuth(l1Bearing - 90), TRIPLINE_RADIUS);
        Point2D tlLeft = gc.getDestinationGeographicPoint();

        TripLine probeTripLine = new TripLine(tlRight, tlLeft, wayId, gf);
        return probeTripLine;
    }

}
