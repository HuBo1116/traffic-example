package com.grab.engine.enrich;

import com.grab.kinesis.model.Location;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hubo on 16/4/22.
 */
public class EnrichEngineExecuter {
    private ExecutorService enrichMapExecutor;
    public static ExecutorService processRecordExecutor;
    private static int shardNum = 10;

    public void start(int workerCores,BlockingQueue<Location> locationQueue) {
        this.enrichMapExecutor = Executors.newFixedThreadPool(workerCores);
        this.processRecordExecutor = Executors.newFixedThreadPool(shardNum);

        for(int i = 0; i< workerCores; i++) {
            EnrichEngineWorker worker = new EnrichEngineWorker(locationQueue);
            this.enrichMapExecutor.execute(worker);
        }
    }
}
