package com.grab.engine.enrich;

import com.grab.engine.geom.Crossing;
import com.grab.engine.geom.GPSPoint;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.algorithm.match.HausdorffSimilarityMeasure;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hubo on 16/4/21.
 */
public class PendingTrack {
    public static double SIMILARITY_COUNT_THRESHOLD = 0.6;
    public static double SIMILARITY_SAME_THRESHOLD = 0.9;
    public static HausdorffSimilarityMeasure HAUSDORFF = new HausdorffSimilarityMeasure();
    private int id;
    private String vehicleId;
    private GPSPoint[] pendingPoints;
    private LineString pendingPointsGeom;
    private Crossing startCrossing;
    private Crossing endCrossing;
    private LineString optimizedPointsGeom;

    public PendingTrack() {
    }

    public PendingTrack(GPSPoint[] potentianlPoints,Crossing lastCrossing,Crossing nextCrossing) {
        if (potentianlPoints.length > 0){
            this.pendingPoints = potentianlPoints;
            this.vehicleId = potentianlPoints[0].getVehicleId();
            Coordinate[] coords = new Coordinate[potentianlPoints.length];
            for (int i=0; i< potentianlPoints.length;i++){
                coords[i] = new Coordinate(potentianlPoints[i].getLon(), potentianlPoints[i].getLat());
            }
            this.pendingPointsGeom = new GeometryFactory().createLineString(coords);
            this.startCrossing = lastCrossing;
            this.endCrossing = nextCrossing;
        }
    }

    public static boolean isSimilarity(PendingTrack a, PendingTrack b, double similarityThreshold){
        double measureValue = HAUSDORFF.measure(a.getPendingPointsGeom(), b.getPendingPointsGeom());
        return measureValue >= similarityThreshold;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[PendingTrackDao:");
        sb.append("dirveriD:").append(vehicleId).append(",");
        sb.append("startWayId:").append(startCrossing.getTripline().getWayId()).append(",");
        for (GPSPoint point:this.pendingPoints) {
            sb.append("(").append(point.getLat()).append(",").append(point.getLon()).append("),");
        }
        sb.append("endWayId").append(endCrossing.getTripline().getWayId());
        sb.append("]");
        return sb.toString();
    }

    public String toStringDebug() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("[PendingTrackDao vehicleId:");
        sb.append(vehicleId);
        sb.append(", ");
        for (GPSPoint point:this.pendingPoints) {
            Date timestamp = new Date(point.getTime()/1000l);
            sb.append("(").append(point.getLat()).append(",").append(point.getLon()).append(",time:").append(sdfDate.format(timestamp)).append(",accuracy:").append(point.getAccuracy());
            sb.append(",speed:").append(point.getSpeed()).append(",bearing:").append(point.getBearing()).append(")");
        }
        sb.append("]");
        return sb.toString();
    }

    public GPSPoint[] getPendingPoints() {
        return pendingPoints;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public Crossing getStartCrossing() {
        return startCrossing;
    }

    public Crossing getEndCrossing() {
        return endCrossing;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPendingPoints(GPSPoint[] pendingPoints) {
        this.pendingPoints = pendingPoints;
    }

    public LineString getOptimizedPointsGeom() {
        return optimizedPointsGeom;
    }

    public void setOptimizedPointsGeom(LineString optimizedPointsGeom) {
        this.optimizedPointsGeom = optimizedPointsGeom;
    }

    public LineString getPendingPointsGeom() {
        return pendingPointsGeom;
    }

    public void setPendingPointsGeom(LineString pendingPointsGeom) {
        this.pendingPointsGeom = pendingPointsGeom;
    }
}
