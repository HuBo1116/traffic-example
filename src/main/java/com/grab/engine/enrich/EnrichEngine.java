package com.grab.engine.enrich;

import com.conveyal.osmlib.OSM;
import com.conveyal.osmlib.Way;
import com.grab.app.EnrichMap;
import com.grab.engine.geom.Crossing;
import com.grab.engine.geom.GPSPoint;
import com.grab.engine.geom.GPSSegment;
import com.grab.engine.geom.TripLine;
import com.grab.engine.osm.OSMUtils;
import com.grab.external.google.SnapToRoad;
import com.grab.external.google.SnapToRoadLocation;
import com.grab.postgis.PendingTrackDao;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.index.quadtree.Quadtree;
import com.vividsolutions.jts.linearref.LengthIndexedLine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by hubo on 16/4/20.
 */
public class EnrichEngine {

    private static final Log logger = LogFactory.getLog(EnrichEngine.class);
    // The unit for split the road. along a road.
    private static final double SEGMENT_LENGTH_METERS = 20;

    // Max time between two successive GPS fixes from a single vehicle. Anything longer is considered noise.
    private static final int MAX_GPS_PAIR_DURATION = 20;

    private static final int VALID_STREAK_NUMBER = 3;

    // for enrich carMap tripline
    public Quadtree enrichMapQuadtree = new Quadtree();

    private GeometryFactory geometryFactory = new GeometryFactory();

    //====VEHICLE STATE=====
    // Vehicle id -> last encountered GPS fix
    Map<String, GPSPoint> lastPoint = new HashMap<>();
    // Vehicle id -> all pending points
    Map<String, List<GPSPoint>> pendingPoints = new HashMap<>();
    // Vehicle id -> last passed through the probe barrier
    Map<String, Crossing> lastCrossing = new HashMap<>();


    public void addProbeBarrier(OSM osm) {
        // for each way
        for (Map.Entry<Long, Way> wayEntry : osm.ways.entrySet()) {
            long wayId = wayEntry.getKey();
            Way way = wayEntry.getValue();

            if (!OSMUtils.checkIsAcceptableHighway(way)){
                continue;
            }

            LineString wayPath;
            try {
                wayPath = OSMUtils.getLineStringForWay(way, osm);
            } catch (RuntimeException ex) {
                continue;
            }
            addBarrierAlongWay(wayId, wayPath);
        }
    }

    private void addBarrierAlongWay(long wayId, LineString wayPath) {
        double wayLen = OSMUtils.getLength(wayPath); // meters
        LengthIndexedLine indexedWayPath = new LengthIndexedLine(wayPath);

        double startIndex = indexedWayPath.getStartIndex();
        double endIndex = indexedWayPath.getEndIndex();

        // find topological units per meter
        double scale = (endIndex - startIndex) / wayLen; // topos/meter
        // meters * topos/meter = topos
        double indexMargin = SEGMENT_LENGTH_METERS * scale;

        for (double i = startIndex; i < endIndex; i += indexMargin) {
            TripLine probe = OSMUtils.genProbeTripline(wayId,indexedWayPath,i,geometryFactory);
            enrichMapQuadtree.insert(probe.getEnvelope(),probe);
        }
    }

    /**
     * Update the enrich engine with a new GPS fix. If the GPS fix didn't intersect any probe barriers,
     * it returns false as in a potential missing road.
     *
     * @param gpsPoint
     * @return
     */
    public PendingTrack update(GPSPoint gpsPoint) {
        PendingTrack potenial = null;

        GPSSegment gpsSegment = getGpsSegment(gpsPoint);
        if (gpsSegment == null){ //cant judge, just let it go
            return potenial;
        }

        List<GPSPoint> vehiclePendingPoints = this.pendingPoints.get(gpsPoint.getVehicleId());
        if (vehiclePendingPoints == null) {
            vehiclePendingPoints = new ArrayList<>();
            pendingPoints.put(gpsPoint.getVehicleId(),vehiclePendingPoints);

        }
        Crossing crossing = getLastCrossing(gpsSegment);
        if (crossing == null) {
            vehiclePendingPoints.add(gpsPoint);
        }else {
            if (vehiclePendingPoints.size() >= VALID_STREAK_NUMBER && isAvailableGPSPoints(vehiclePendingPoints)) {
                GPSPoint[] gpsPoints = vehiclePendingPoints.toArray(new GPSPoint[vehiclePendingPoints.size()]);
                if (lastCrossing.get(gpsPoint.getVehicleId()) != null) {
                    potenial = new PendingTrack(gpsPoints,lastCrossing.get(gpsPoint.getVehicleId()),crossing);
                }
            }
            vehiclePendingPoints.clear();
            lastCrossing.put(gpsPoint.getVehicleId(),crossing);
        }
        return potenial;
    }

    private boolean isAvailableGPSPoints(List<GPSPoint> vehiclePendingPoints){
        return hasAvailablePathLength(vehiclePendingPoints) && hasAvailableAccuracy(vehiclePendingPoints) && !hasSuddenTurn(vehiclePendingPoints);
    }



    private GPSSegment getGpsSegment(GPSPoint gpsPoint) {
        GPSPoint p0 = lastPoint.get(gpsPoint.getVehicleId());
        lastPoint.put(gpsPoint.getVehicleId(), gpsPoint);

        // null if this is the vehicle's first point, cant judge.
        if (p0 == null) {
            return null;
        }

        if(gpsPoint.getTime() - p0.getTime() < 0 || gpsPoint.getTime() - p0.getTime() > MAX_GPS_PAIR_DURATION*1000000){
            return null;
        }

        GPSSegment gpsSegment = new GPSSegment(p0, gpsPoint);

        if (gpsSegment.isStill()) {
            return null;
        }
        return gpsSegment;
    }

    private Crossing getLastCrossing(GPSSegment gpsSegment) {
        Crossing lastCrossing = null;
        List<?> tripLines = enrichMapQuadtree.query(gpsSegment.getEnvelope());
        for (Object tlObj : tripLines) {
            TripLine tl = (TripLine) tlObj;
            Crossing crossing = gpsSegment.getCrossing(tl);
            if (crossing != null) {
                if (lastCrossing == null){
                    lastCrossing = crossing;
                } else if (crossing.getTimeMicros() > lastCrossing.getTimeMicros()){
                    lastCrossing = crossing;
                }
            }
        }
        return lastCrossing;
    }

    public static double getSegmentLengthMeters(){
        return EnrichEngine.SEGMENT_LENGTH_METERS;
    }

    private boolean hasAvailablePathLength(List<GPSPoint> potentianlPoints){
        boolean available = true;

        Coordinate[] coords = new Coordinate[potentianlPoints.size()];
        for (int i=0; i< potentianlPoints.size();i++){
            coords[i] = new Coordinate(potentianlPoints.get(i).getLon(), potentianlPoints.get(i).getLat());
        }

        LineString geom = new GeometryFactory().createLineString(coords);
        double pendingPathLength = OSMUtils.getLength(geom);
        if(pendingPathLength < (potentianlPoints.size()-1)*EnrichEngine.getSegmentLengthMeters()/2){
            available = false;
        }
        return available;
    }

    private boolean hasAvailableAccuracy(List<GPSPoint> potentianlPoints){
        double meanAccuracy = 0;
        for(GPSPoint gps: potentianlPoints){
            meanAccuracy += Double.parseDouble(gps.getAccuracy());
        }
        meanAccuracy = meanAccuracy/potentianlPoints.size();
        return meanAccuracy < MAX_GPS_PAIR_DURATION/2;
    }

    private boolean hasSuddenTurn(List<GPSPoint> potentianlPoints){
        boolean hasSuddenTurn = false;
        double lastBearing = Double.parseDouble(potentianlPoints.get(0).getBearing());
        for(GPSPoint gps: potentianlPoints){
            if(Math.abs(Double.parseDouble(gps.getBearing()) - lastBearing) >  150) {
                hasSuddenTurn = true;
                break;
            }
        }
        return hasSuddenTurn;
    }

    public static void batchOptimizePendingTrack() {
        PendingTrackDao dao = new PendingTrackDao();
        List<PendingTrack> pendingTrackList = dao.findToday();
        if(!pendingTrackList.isEmpty()){
            Map<PendingTrack, Integer> sortedMap = getSortedMapBySimilarity(pendingTrackList);
            callGoogleSnapForOptimization(dao, sortedMap);
        }
    }

    private static Map<PendingTrack, Integer> getSortedMapBySimilarity(List<PendingTrack> pendingTrackList) {
        Map<PendingTrack,Integer> similarityMeasureMap = new HashMap<>();
        for(PendingTrack pendingTrack: pendingTrackList){
            int similarityCount = 0;
            for(PendingTrack otherPendingTrack: pendingTrackList){
                if (pendingTrack != otherPendingTrack && PendingTrack.isSimilarity(pendingTrack, otherPendingTrack,PendingTrack.SIMILARITY_COUNT_THRESHOLD)) {
                    similarityCount++;
                }
            }
            if (similarityCount > 0) {
                similarityMeasureMap.put(pendingTrack,similarityCount);
            }
        }
        similarityMeasureMap = sortByValue(similarityMeasureMap);
        return similarityMeasureMap;
    }

    private static void callGoogleSnapForOptimization(PendingTrackDao dao, Map<PendingTrack, Integer> sortedMap) {
        PendingTrack lastOptimizedTrack = null;
        int callCount = 1;
        for(PendingTrack pendingTrack : sortedMap.keySet()){
            try {
                if (lastOptimizedTrack != null && PendingTrack.isSimilarity(pendingTrack, lastOptimizedTrack, PendingTrack.SIMILARITY_SAME_THRESHOLD)) {
                    continue;
                }
                lastOptimizedTrack = pendingTrack;
                updateOptimizedTrack(dao, pendingTrack);
            } catch (IOException ex) {
                ex.printStackTrace();
                logger.error("google snap to road failed caused by:", ex);
            }
            if (callCount < SnapToRoad.GOOGLE_SNAP_TO_ROAD_LIMIT) {
                callCount++;
            }else {
                logger.error("========== Call Google SnapToRoad Count:" + callCount + " ==========");
                return;
            }
        }
    }

    private static void updateOptimizedTrack(PendingTrackDao dao, PendingTrack pendingTrack) throws IOException {
        List<SnapToRoadLocation> optimizedPointsList = SnapToRoad.getGoogleSnappedTrack(pendingTrack);
        LineString optimizedTrack = createLineStringBySnapToRoadLocation(optimizedPointsList);
        logger.info("optimizedTrack linestring is:" + optimizedTrack.toString());
        if (optimizedTrack != null) {
            pendingTrack.setOptimizedPointsGeom(optimizedTrack);
            dao.updateOptimizedTrack(pendingTrack);
        }
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list){
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    private static LineString createLineStringBySnapToRoadLocation(List<SnapToRoadLocation> snapToRoadLocationList) {
        if(snapToRoadLocationList.isEmpty() || snapToRoadLocationList.size() <= 2) {
            return null;
        }
        Coordinate[] coords = new Coordinate[snapToRoadLocationList.size()];
        for (int i=0; i< snapToRoadLocationList.size();i++){
            coords[i] = new Coordinate(snapToRoadLocationList.get(i).getLongitude(), snapToRoadLocationList.get(i).getLatitude());
        }
        return new GeometryFactory().createLineString(coords);
    }

    public static void batchScheduleOptimizeTrack(){
        Runnable runnable = () -> batchOptimizePendingTrack();
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();
        Integer startHour;
        Integer startMin;
        try {
            startHour = Integer.parseInt(EnrichMap.appProps.getProperty("application.enrich.batch.update.hour"));
            startMin = Integer.parseInt(EnrichMap.appProps.getProperty("application.enrich.batch.update.min"));
        } catch(Exception e) {
            startHour = 24;
            startMin = 0;
            logger.info("Property application.enrich.batch.update.hour not set.");
        }
        calendar.set(Calendar.HOUR_OF_DAY,startHour);
        calendar.set(Calendar.MINUTE, startMin);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long start = calendar.getTimeInMillis();
        long initialDelay = (start - now)/1000;
        service.scheduleAtFixedRate(runnable, initialDelay , 60 * 60 * 24, TimeUnit.SECONDS);
    }
}
