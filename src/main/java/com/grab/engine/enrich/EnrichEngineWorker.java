package com.grab.engine.enrich;

import com.grab.app.EnrichMap;
import com.grab.engine.common.Constant;
import com.grab.engine.geom.GPSPoint;
import com.grab.kinesis.model.Location;
import com.grab.postgis.PendingTrackDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by hubo on 16/4/22.
 */
public class EnrichEngineWorker implements Runnable{
    private static final Log logger = LogFactory.getLog(EnrichEngineWorker.class);
    private BlockingQueue<Location> locationQueue;

    public EnrichEngineWorker(BlockingQueue<Location> locationQueue){
        this.locationQueue = locationQueue;
    }

    @Override
    public void run() {
        while(true) {
            try{
                Location location = this.locationQueue.poll(10, TimeUnit.MILLISECONDS);
                if (location != null && isInSingapore(location)) {
                    this.updateEnrich(location);
                }
            }catch (Exception ex){
                logger.error("error is:", ex);
            }
        }
    }

    private void updateEnrich(Location location) {
        try {
            GPSPoint pt = new GPSPoint(location);
            PendingTrack pendingTrack = EnrichMap.enrichEngine.update(pt);
            if(pendingTrack != null){
                PendingTrackDao dao = new PendingTrackDao();
                dao.save(pendingTrack);
            }
        } catch (Exception ex) {
            logger.error("update error is:", ex);
        }
    }

    private boolean isInVietnam(Location location) {
        String cityCode = EnrichMap.cityCodeMap.get(location.getTaxiTypeId());
        return cityCode != null && Constant.VIETNAM_CITY_CODE.contains(cityCode);
    }

    private boolean isInSingapore(Location location) {
        String cityCode = EnrichMap.cityCodeMap.get(location.getTaxiTypeId());
        return cityCode != null && Constant.SINGAPORE_CITY_CODE.contains(cityCode);
    }
}
