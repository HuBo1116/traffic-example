package com.grab.engine.geom;


/**
 * Created by hubo on 16/4/19.
 */
public class Crossing {
    public GPSSegment gpsSegment;
    public TripLine tripline;
    public long timeMicros;

    public Crossing(GPSSegment gpsSegment, TripLine tl, long time) {
        this.gpsSegment = gpsSegment;
        this.tripline = tl;
        this.timeMicros = time;
    }

    public String toString() {
        return "vehicle " + gpsSegment.p0.getVehicleId() + " crossed " + tripline + " at " + timeMicros;
    }

    public double getTime() {
        return timeMicros / 1000000.0;
    }

    public boolean completedBy(Crossing nextCrossing) {
        if(this.tripline.wayId != nextCrossing.tripline.wayId){
            return false;
        }

        if(Math.abs(this.tripline.tlClusterIndex - nextCrossing.tripline.tlClusterIndex) != 1) {
            return false;
        }

        return true;
    }

    public TripLine getTripline() {
        return this.tripline;
    }

    public long getTimeMicros() {
        return this.timeMicros;
    }
}
