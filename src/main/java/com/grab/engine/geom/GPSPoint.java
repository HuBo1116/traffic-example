package com.grab.engine.geom;

import com.grab.hadoop.model.RedshiftDriverLocation;
import com.grab.kinesis.model.Location;

/**
 * Created by hubo on 16/4/19.
 */
public class GPSPoint {
    private long time;
    private String vehicleId;
    private double lon;
    private double lat;
    private String accuracy;
    private String bearing;
    private double speed;

    public GPSPoint(Location location) {
        String timeStr = location.getTs() + "";
        if (timeStr.length() > 10) {
            timeStr = timeStr.substring(0, 10);
        }
        long time = Long.parseLong(timeStr + "000000"); //from second to microsecond;
        this.time = time;
        this.vehicleId = location.getDriverID();
        this.lon = location.getLng();
        this.lat = location.getLat();
        this.accuracy = location.getAccuracy();
        this.bearing = location.getBearing();
        this.speed = location.getSpeed();
    }

    public GPSPoint(RedshiftDriverLocation location) {
        String timeStr = location.getLoggedTime().getTime() + "";
        this.time = Long.parseLong(timeStr + "000");
        this.vehicleId = location.getDriverID()+"";
        this.lon = location.getLon();
        this.lat = location.getLat();
        this.accuracy = location.getAccuracy() + "";
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    public long getTime() {
        return time;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public String getBearing() {
        return bearing;
    }

    public double getSpeed() {
        return speed;
    }
}
