package com.grab.engine.geom;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.linearref.LengthIndexedLine;

import java.awt.geom.Point2D;

/**
 * Created by hubo on 16/4/19.
 */
public class TripLine {
    public long wayId;
    public int ndIndex; // the node along the way associated with this tripline
    public int tlIndex; // the tripline along the way
    public int tlClusterIndex; // the nth cluster of triplines along the way. Each node can have zero, one,
    // or two triplines depending on whether it's an endpoint or an intersection.
    // The cluster index is important to keep around because pairs of tripline crossings
    // from consecutive clusters are particularly important for establishing speed
    // along a way segment containing many nodes.

    public double dist;
    public LineString geom;
    public boolean oneway;
    public long ndID; // the node id along the way associated with this tripline


    public TripLine(Point2D left, Point2D right, long wayId, int ndIndex, long ndID, int tlIndex, int tlClusterIndex, double dist, boolean oneway) {
        GeometryFactory gf = new GeometryFactory();

        Coordinate[] coords = new Coordinate[2];
        coords[0] = new Coordinate(left.getX(), left.getY());
        coords[1] = new Coordinate(right.getX(), right.getY());
        this.geom = gf.createLineString(coords);

        this.wayId = wayId;
        this.ndIndex = ndIndex;
        this.tlIndex = tlIndex;
        this.tlClusterIndex = tlClusterIndex;
        this.dist = dist;
        this.oneway = oneway;
        this.ndID = ndID;
    }

    public TripLine(Point2D left, Point2D right, long wayId, GeometryFactory gf) {
        Coordinate[] coords = new Coordinate[2];
        coords[0] = new Coordinate(left.getX(), left.getY());
        coords[1] = new Coordinate(right.getX(), right.getY());
        this.geom = gf.createLineString(coords);

        this.wayId = wayId;
    }

    public String toString() {
        return "[Tripline way:" + wayId + " tlix:" + tlIndex + "]";
    }

    public Envelope getEnvelope() {
        return geom.getEnvelopeInternal();
    }

    public LineSegment getLineSegment() {
        return new LineSegment(geom.getCoordinateN(0), geom.getCoordinateN(1));
    }

    public LineString getGeom() {
        return this.geom;
    }

    public long getWayId() {
        return this.wayId;
    }

    public int getClusterIndex() {
        return this.tlClusterIndex;
    }

    public Object getIdString() {
        return this.wayId+":"+this.ndIndex;
    }

    public int getNdIndex() {
        return this.ndIndex;
    }

    public long getNdID() {
        return this.ndID;
    }
}
