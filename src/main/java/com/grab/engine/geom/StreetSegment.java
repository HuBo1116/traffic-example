package com.grab.engine.geom;

import com.conveyal.osmlib.Way;
import com.vividsolutions.jts.geom.Coordinate;

/**
 * Created by hubo on 16/4/19.
 */
public class StreetSegment {
    public Coordinate[] seg;
    public Way way;
    public int start; // first node index
    public int end; // last node index, inclusive
    public long wayId;

    public StreetSegment(Coordinate[] seg, long wayId, Way way, int start, int end) {
        this.seg = seg;
        this.wayId = wayId;
        this.way = way;
        this.start = start;
        this.end = end;
    }
}
