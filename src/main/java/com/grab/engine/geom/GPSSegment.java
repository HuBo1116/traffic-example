package com.grab.engine.geom;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

/**
 * Created by hubo on 16/4/19.
 */
public class GPSSegment {
    private LineString geom;
    GPSPoint p0;
    GPSPoint p1;
    public String vehicleId;

    public GPSSegment(GPSPoint p0, GPSPoint p1) {
        Coordinate[] coords = new Coordinate[2];
        coords[0] = new Coordinate(p0.getLon(), p0.getLat());
        coords[1] = new Coordinate(p1.getLon(), p1.getLat());
        this.geom = new GeometryFactory().createLineString(coords);

        if (!p0.getVehicleId().equals(p1.getVehicleId())) {
            throw new IllegalArgumentException("vehicle ids don't match");
        }

        this.p0 = p0;
        this.p1 = p1;
        this.vehicleId = p0.getVehicleId();
    }

    public Crossing getCrossing(TripLine tl) {
        Double percAlongGpsSegment = this.getLineSegment().intersectionDistance(tl.getLineSegment());

        if (percAlongGpsSegment == null || percAlongGpsSegment < 0 || percAlongGpsSegment > 1) {
            return null;
        }

        Double percAlongTripline = tl.getLineSegment().intersectionDistance(this.getLineSegment());

        if (percAlongTripline == null || percAlongTripline < 0 || percAlongTripline > 1) {
            return null;
        }

        long time = (long) (this.getDuration() * percAlongGpsSegment + p0.getTime());

        return new Crossing(this, tl, time);
    }

    private LineSegment getLineSegment() {
        return new LineSegment(new Coordinate(p0.getLon(), p0.getLat()), new Coordinate(p1.getLon(), p1.getLat()));
    }

    private long getDuration() {
        // segment duration in milliseconds

        return p1.getTime() - p0.getTime();
    }

    public Envelope getEnvelope() {
        return geom.getEnvelopeInternal();
    }

    public boolean isStill() {
        return p0.getLat() == p1.getLat() && p0.getLon() == p1.getLon();
    }
}
