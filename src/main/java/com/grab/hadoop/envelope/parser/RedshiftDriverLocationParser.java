package com.grab.hadoop.envelope.parser;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.RedshiftDriverLocation;

import java.text.SimpleDateFormat;

/**
 * Created by hubo on 16/6/1.
 */
public class RedshiftDriverLocationParser {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static RedshiftDriverLocation parseRedshiftData(String item){
        RedshiftDriverLocation driverLocation = new RedshiftDriverLocation();
        try{
            String[] fields = item.split(Constant.S3_DRIVER_LOCATION_SEPARATOR);
            if(fields.length == Constant.S3_DRIVER_LOCATION_VALID_LENGTH){
                driverLocation.setLoggedTimeString(fields[0]);
                driverLocation.setDriverID(Long.parseLong(fields[1]));
                driverLocation.setAccuracy(Double.parseDouble(fields[5]));
                driverLocation.setLat(Double.parseDouble(fields[9]));
                driverLocation.setLon(Double.parseDouble(fields[10]));
//                driverLocation.setTaxiTypeID(Long.parseLong(fields[12]));
                driverLocation.setLoggedTime(dateFormat.parse(fields[0]));
                driverLocation.setAccuracy(Constant.VALID_ACCURACY_THRESHOLD);
            }else{
                driverLocation = null;
            }
        }catch(Exception e){
            driverLocation = null;
        }
        return driverLocation;
    }

    public static RedshiftDriverLocation parseRedshiftETAData(String item){
        RedshiftDriverLocation driverLocation = new RedshiftDriverLocation();
        try{
            String[] fields = item.split(Constant.S3_DRIVER_LOCATION_SEPARATOR);
            if(fields.length == Constant.S3_DRIVER_LOCATION_MINI_VALID_LENGTH){
                driverLocation.setLoggedTimeString(fields[1]);
                driverLocation.setDriverID(Long.parseLong(fields[0]));
                driverLocation.setLat(Double.parseDouble(fields[2]));
                driverLocation.setLon(Double.parseDouble(fields[3]));
                driverLocation.setLoggedTime(dateFormat.parse(fields[1]));
            }else{
                driverLocation = null;
            }
        }catch(Exception e){
            driverLocation = null;
        }
        return driverLocation;
    }

    public static RedshiftDriverLocation parseMeanSpeedJobMapResult(String item){
        RedshiftDriverLocation driverLocation = new RedshiftDriverLocation();
        try{
            String[] fields = item.split(Constant.RESULT_SEPARATOR);
            if(fields.length == Constant.SPEED_MAP_RESULT_VALID_LENGTH){
                driverLocation.setDriverID(Long.parseLong(fields[0]));
                driverLocation.setLoggedTime(dateFormat.parse(fields[1]));
                driverLocation.setLat(Double.parseDouble(fields[2]));
                driverLocation.setLon(Double.parseDouble(fields[3]));
                driverLocation.setAccuracy(Double.parseDouble(fields[4]));
            }else{
                driverLocation = null;
            }
        }catch(Exception e){
            driverLocation = null;
        }
        return driverLocation;
    }


}
