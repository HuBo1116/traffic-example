package com.grab.hadoop.envelope.parser;

import com.grab.engine.common.TimeUtil;
import com.grab.engine.geom.SpeedSample;
import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.RedshiftDriverLocation;

import java.math.BigDecimal;

/**
 * Created by hubo on 16/6/1.
 */
public class RedshiftDriverLocationResultAssembler {
    private static double TRANSFORM_TO_KM_PER_HOUR = 3.6; //from m/s to km/h

    public static String assembleSpeedMapResult(RedshiftDriverLocation driverLocation){
        StringBuilder sb = new StringBuilder();
        sb.append(driverLocation.getDriverID()).append(Constant.RESULT_SEPARATOR);
        sb.append(driverLocation.getLoggedTimeString()).append(Constant.RESULT_SEPARATOR);
        sb.append(driverLocation.getLat()).append(Constant.RESULT_SEPARATOR);
        sb.append(driverLocation.getLon()).append(Constant.RESULT_SEPARATOR);
        sb.append(driverLocation.getAccuracy());
        return sb.toString();
    }

    public static long assembleSpeedReduceKeyResult(SpeedSample speedSample){
        return speedSample.getFirstCrossing().getTripline().getWayId();
    }

    public static String assembleSpeedReduceValueResult(SpeedSample speedSample){
        StringBuilder sb = new StringBuilder();
        sb.append(speedSample.getFirstCrossing().getTripline().getNdID()).append(Constant.RESULT_SEPARATOR);
        sb.append(speedSample.getLastCrossing().getTripline().getNdID()).append(Constant.RESULT_SEPARATOR);
        sb.append(TimeUtil.getHourOfWeek(speedSample.getLastCrossing().getTimeMicros())).append(Constant.RESULT_SEPARATOR);
        sb.append(new BigDecimal(speedSample.getSpeed() * TRANSFORM_TO_KM_PER_HOUR).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
        return sb.toString();
    }
}
