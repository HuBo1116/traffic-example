package com.grab.hadoop.envelope.parser;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.MeanSpeed;

/**
 * Created by hubo on 16/6/3.
 */
public class MeanSpeedAssembler {
    public static String assembleMeanMapKey(MeanSpeed meanSpeed){
        StringBuilder sb = new StringBuilder();
        sb.append(meanSpeed.getStartNodeID()).append(Constant.RESULT_SEPARATOR);
        sb.append(meanSpeed.getEndNodeID()).append(Constant.RESULT_SEPARATOR);
        sb.append(meanSpeed.getHourOfWeek());
        return sb.toString();
    }
}
