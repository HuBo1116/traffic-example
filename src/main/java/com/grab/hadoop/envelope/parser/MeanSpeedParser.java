package com.grab.hadoop.envelope.parser;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.MeanSpeed;

/**
 * Created by hubo on 16/6/1.
 */
public class MeanSpeedParser {

    public static MeanSpeed parseSpeedJobResult(String item){
        MeanSpeed meanSpeed = new MeanSpeed();
        try{
            String[] fields = item.split(Constant.RESULT_SEPARATOR);
            if(fields.length == Constant.SPEED_JOB_RESULT_LENGTH){
                meanSpeed.setWayID(Long.parseLong(fields[0]));
                meanSpeed.setStartNodeID(Long.parseLong(fields[1]));
                meanSpeed.setEndNodeID(Long.parseLong(fields[2]));
                meanSpeed.setHourOfWeek(Integer.parseInt(fields[3]));
                meanSpeed.setSpeed(Integer.parseInt(fields[4]));
            }else{
                meanSpeed = null;
            }
        }catch(Exception e){
            meanSpeed = null;
        }
        return meanSpeed;
    }
}
