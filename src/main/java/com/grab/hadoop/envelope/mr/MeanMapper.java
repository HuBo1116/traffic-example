package com.grab.hadoop.envelope.mr;

import com.grab.hadoop.model.MeanSpeed;
import com.grab.hadoop.envelope.parser.MeanSpeedAssembler;
import com.grab.hadoop.envelope.parser.MeanSpeedParser;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hubo on 16/6/3.
 */
public class MeanMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private Text wayInfo = new Text();
    private IntWritable speed = new IntWritable();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        MeanSpeed meanSpeed = MeanSpeedParser.parseSpeedJobResult(value.toString());
        if (meanSpeed !=null) {
            wayInfo.set(MeanSpeedAssembler.assembleMeanMapKey(meanSpeed));
            speed.set(meanSpeed.getSpeed());
            context.write(wayInfo, speed);
        }
    }
}
