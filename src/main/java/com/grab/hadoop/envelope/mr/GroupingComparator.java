package com.grab.hadoop.envelope.mr;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;


/**
 * Created by hubo on 16/6/2.
 */
public class GroupingComparator extends WritableComparator{
    protected GroupingComparator() {
        super(LongPair.class, true);
    }

    @Override
    public int compare(WritableComparable w1, WritableComparable w2) {
        LongPair ip1 = (LongPair) w1;
        LongPair ip2 = (LongPair) w2;
        return ip1.getDriverID() == ip2.getDriverID() ? 0 : (ip1.getDriverID() < ip2.getDriverID() ? -1 : 1);
    }
}
