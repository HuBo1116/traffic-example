package com.grab.hadoop.envelope.mr;

import com.conveyal.osmlib.OSM;
import com.grab.engine.geom.GPSPoint;
import com.grab.engine.geom.SpeedSample;
import com.grab.engine.traffic.TrafficEngine;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationParser;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationResultAssembler;
import com.grab.hadoop.model.RedshiftDriverLocation;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.List;

/**
 * Created by hubo on 16/6/1.
 */
public class SpeedReducer extends Reducer<LongPair, Text, LongWritable, Text> {
    private Text result = new Text();
    private LongWritable wayID = new LongWritable();
    private LongWritable driverID = new LongWritable();
    static OSM osm;
    static TrafficEngine trafficEngine;

    @SuppressWarnings("unchecked")
    protected void setup(Context context) throws IOException, InterruptedException {
        startOSM();
        startTrafficEngine();
    }

    @Override
    public void reduce(LongPair key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            RedshiftDriverLocation driverLocation = RedshiftDriverLocationParser.parseMeanSpeedJobMapResult(value.toString());
            if (driverLocation != null) {
                GPSPoint pt = new GPSPoint(driverLocation);
                List<SpeedSample> speeds = trafficEngine.update(pt);
                if(speeds != null && !speeds.isEmpty()){
                    for(SpeedSample ss : speeds){
                        wayID.set(RedshiftDriverLocationResultAssembler.assembleSpeedReduceKeyResult(ss));
                        result.set(RedshiftDriverLocationResultAssembler.assembleSpeedReduceValueResult(ss));
                        context.write(wayID, result);
                    }
                }
                context.write(driverID, result);
            }
        }
    }

    private void startOSM() {
        osm = new OSM(null);
        osm.readFromFile("data/map.pbf");
    }

    private void startTrafficEngine() {
        trafficEngine = new TrafficEngine();
        trafficEngine.setStreets(osm);
    }
}
