package com.grab.hadoop.envelope.mr;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.RedshiftDriverLocation;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationParser;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationResultAssembler;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hubo on 16/5/31.
 */
public class SpeedMapper extends Mapper<LongWritable, Text, LongPair, Text> {
    private LongPair driverTimestampKey = new LongPair();
    private Text locationInfo = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        RedshiftDriverLocation driverLocation = RedshiftDriverLocationParser.parseRedshiftData(value.toString());

        if (driverLocation !=null && driverLocation.getAccuracy() <= Constant.VALID_ACCURACY_THRESHOLD) {
            driverTimestampKey.set(driverLocation.getDriverID(), driverLocation.getLoggedTime().getTime());
            locationInfo.set(RedshiftDriverLocationResultAssembler.assembleSpeedMapResult(driverLocation));

            context.write(driverTimestampKey, locationInfo);
        }
    }
}
