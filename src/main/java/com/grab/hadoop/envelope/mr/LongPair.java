package com.grab.hadoop.envelope.mr;

import com.grab.hadoop.common.Constant;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by hubo on 16/6/2.
 */
public class LongPair implements WritableComparable<LongPair> {
    long driverID;
    long timestamp;

    @Override
    public int compareTo(LongPair o) {
        if (driverID != o.driverID) {
            return driverID < o.driverID ? -1 : 1;
        }else if (timestamp != o.timestamp) {
            return timestamp < o.timestamp ? -1 : 1;
        }else {
            return 0;
        }
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(driverID);
        dataOutput.writeLong(timestamp);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        driverID = dataInput.readLong();
        timestamp = dataInput.readLong();
    }

    @Override
    public String toString() {
        return this.driverID + Constant.RESULT_SEPARATOR + this.timestamp;
    }

    public long getDriverID() {
        return driverID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void set(long driverID, long timestamp) {
        this.driverID = driverID;
        this.timestamp = timestamp;
    }

    @Override
    //The hashCode() method is used by the HashPartitioner (the default partitioner in MapReduce)
    public int hashCode(){
        return Integer.parseInt(driverID * 157 + timestamp + "");
    }
    @Override
    public boolean equals(Object o) {
        if (o == null){
            return false;
        }
        if (this == o) {
            return true;
        }
        if (o instanceof LongPair){
            LongPair longPair = (LongPair) o;
            return driverID == longPair.driverID  && timestamp == longPair.timestamp;
        }else{
            return false;
        }
    }
}
