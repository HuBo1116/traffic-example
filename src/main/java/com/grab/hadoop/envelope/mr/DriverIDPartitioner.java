package com.grab.hadoop.envelope.mr;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * Created by hubo on 16/6/2.
 */
public class DriverIDPartitioner extends Partitioner<LongPair,Text> {
    @Override
    public int getPartition(LongPair key, Text value, int numPartitions) {
        return Math.abs((int)key.getDriverID() * 127) % numPartitions;
    }
}
