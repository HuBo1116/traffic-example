package com.grab.hadoop.envelope.mr;

import com.grab.hadoop.common.Constant;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by hubo on 16/6/1.
 */
public class MeanReducer extends Reducer<Text, IntWritable, Text, Text> {
    private Text result = new Text();

    @Override
    public void reduce(Text key, Iterable<IntWritable> values,Context context) throws IOException, InterruptedException {
        int count = 0;
        int speedSum = 0;
        for (IntWritable speed : values) {
            count++;
            speedSum += speed.get();
        }
        if (Math.floorDiv(speedSum,count) > 0) {
            result.set(count + Constant.RESULT_SEPARATOR + new BigDecimal(speedSum/count).setScale(0,BigDecimal.ROUND_HALF_UP).intValue());
            context.write(key, result);
        }
    }
}
