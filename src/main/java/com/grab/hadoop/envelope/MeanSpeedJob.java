package com.grab.hadoop.envelope;

import com.grab.hadoop.envelope.mr.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Created by hubo on 16/6/1.
 */
public class MeanSpeedJob {
    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.err.println("Usage: MaxTemperature <input path> <output path>");
            System.exit(-1);
        }
        Path tempPath = new Path(args[1]+"/tmp");

        Configuration conf = new Configuration();

        Job speedJob = new Job(conf,"mean speed step 1");

        speedJob.setMapperClass(SpeedMapper.class);
        speedJob.setReducerClass(SpeedReducer.class);

        speedJob.setPartitionerClass(DriverIDPartitioner.class);
        speedJob.setGroupingComparatorClass(GroupingComparator.class);

        speedJob.setMapOutputKeyClass(LongPair.class);
        speedJob.setMapOutputValueClass(Text.class);

        speedJob.setOutputKeyClass(LongWritable.class);
        speedJob.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(speedJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(speedJob, tempPath);

        speedJob.waitForCompletion(true);

        Job statJob = new Job(conf,"mean speed step 2");

        statJob.setMapperClass(MeanMapper.class);
        statJob.setReducerClass(MeanReducer.class);

        statJob.setMapOutputKeyClass(Text.class);
        statJob.setMapOutputValueClass(IntWritable.class);

        statJob.setOutputKeyClass(Text.class);
        statJob.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(statJob, tempPath);
        FileOutputFormat.setOutputPath(statJob, new Path(args[1]+"/mean-speed"));
        statJob.waitForCompletion(true);

    }
}
