package com.grab.hadoop.model;

/**
 * Created by hubo on 16/6/3.
 */
public class MeanSpeed {
    private long driverID;
    private long wayID;
    private long startNodeID;
    private long endNodeID;
    private int hourOfWeek;
    private int speed;
    private String nodeSet;
    private double avgSpeed;
    private double distance;

    public long getWayID() {
        return wayID;
    }

    public void setWayID(long wayID) {
        this.wayID = wayID;
    }

    public long getStartNodeID() {
        return startNodeID;
    }

    public void setStartNodeID(long startNodeID) {
        this.startNodeID = startNodeID;
    }

    public long getEndNodeID() {
        return endNodeID;
    }

    public void setEndNodeID(long endNodeID) {
        this.endNodeID = endNodeID;
    }

    public int getHourOfWeek() {
        return hourOfWeek;
    }

    public void setHourOfWeek(int hourOfWeek) {
        this.hourOfWeek = hourOfWeek;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getNodeSet() {
        return nodeSet;
    }

    public void setNodeSet(String nodeSet) {
        this.nodeSet = nodeSet;
    }

    public double getAvgSpeed() {
        return avgSpeed;
    }

    public void setAvgSpeed(double avgSpeed) {
        this.avgSpeed = avgSpeed;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getDriverID() {
        return driverID;
    }

    public void setDriverID(long driverID) {
        this.driverID = driverID;
    }
}
