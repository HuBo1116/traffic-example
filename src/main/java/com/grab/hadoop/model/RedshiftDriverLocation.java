package com.grab.hadoop.model;

import java.util.Date;

/**
 * Created by hubo on 16/6/1.
 */
public class RedshiftDriverLocation {
    private Date loggedTime;
    private long driverID;
    private double accuracy;
    private double lat;
    private double lon;
    private long taxiTypeID;
    private String loggedTimeString;
    private int internalDriverID;

    public Date getLoggedTime() {
        return loggedTime;
    }

    public void setLoggedTime(Date loggedTime) {
        this.loggedTime = loggedTime;
    }

    public long getDriverID() {
        return driverID;
    }

    public void setDriverID(long driverID) {
        this.driverID = driverID;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public long getTaxiTypeID() {
        return taxiTypeID;
    }

    public void setTaxiTypeID(long taxiTypeID) {
        this.taxiTypeID = taxiTypeID;
    }

    public String getLoggedTimeString() {
        return loggedTimeString;
    }

    public void setLoggedTimeString(String loggedTimeString) {
        this.loggedTimeString = loggedTimeString;
    }

    public int getInternalDriverID() {
        return internalDriverID;
    }

    public void setInternalDriverID(int internalDriverID) {
        this.internalDriverID = internalDriverID;
    }
}
