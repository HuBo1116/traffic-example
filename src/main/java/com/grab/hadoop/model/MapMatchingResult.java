package com.grab.hadoop.model;

import java.util.List;

/**
 * Created by hubo on 16/8/4.
 */
public class MapMatchingResult {
    private String originCoordinates;
    private String snappedCoordinates;
    private String nodeToNodeList;
    private Long osmWayId;
    private List<Long> nodes;

    public String getOriginCoordinates() {
        return originCoordinates;
    }

    public void setOriginCoordinates(String originCoordinates) {
        this.originCoordinates = originCoordinates;
    }

    public String getSnappedCoordinates() {
        return snappedCoordinates;
    }

    public void setSnappedCoordinates(String snappedCoordinates) {
        this.snappedCoordinates = snappedCoordinates;
    }

    public String getNodeToNodeList() {
        return nodeToNodeList;
    }

    public void setNodeToNodeList(String nodeToNodeList) {
        this.nodeToNodeList = nodeToNodeList;
    }

    public List<Long> getNodes() {
        return nodes;
    }

    public void setNodes(List<Long> nodes) {
        this.nodes = nodes;
    }

    public Long getOsmWayId() {
        return osmWayId;
    }

    public void setOsmWayId(Long osmWayId) {
        this.osmWayId = osmWayId;
    }
}
