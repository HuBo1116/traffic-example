package com.grab.hadoop.common;

/**
 * Created by hubo on 16/6/1.
 */
public class Constant {
    public static final String S3_DRIVER_LOCATION_SEPARATOR = "\\|";
    public static final String RESULT_SEPARATOR = "\u0009";
    public static final int S3_DRIVER_LOCATION_VALID_LENGTH = 13;
    public static final int S3_DRIVER_LOCATION_MINI_VALID_LENGTH = 4;
    public static final int SPEED_MAP_RESULT_VALID_LENGTH = 5;
    public static final double VALID_ACCURACY_THRESHOLD = 15.0;
    public static final int SPEED_JOB_RESULT_LENGTH = 5;
    public static final int HMM_SPEED_JOB_RESULT_LENGTH = 13;
}


