package com.grab.hadoop.common;

/**
 * Created by hubo on 16/6/1.
 */
public class InvalidDataException extends Exception{
    public InvalidDataException(String msg) {
        super(msg);
    }
}
