package com.grab.hadoop.hmm.mr;

/**
 * Created by hubo on 16/8/3.
 */

import com.conveyal.osmlib.OSM;
import com.conveyal.osmlib.Way;
import com.grab.hadoop.common.Constant;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WayCoverageReducer extends Reducer<LongWritable, Text, LongWritable, Text> {
    private Text result = new Text();
    private static OSM osm;
    private int totalNodePair;
    private int totalNodePairReserve;
    private int bingoNodePair;
    private int bingoNodePairReserve;

    @SuppressWarnings("unchecked")
    protected void setup(Context context) throws IOException, InterruptedException {
        loadOsmData();
    }

    @Override
    public void reduce(LongWritable key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
        Way way = osm.ways.get(key.get());
        Map<String,Boolean> wayAllNodesMap = new HashMap<>();
        Map<String,Boolean> wayAllNodesReserveMap = new HashMap<>();
        for (int i=0; i < way.nodes.length-1; i++) {
            wayAllNodesMap.put(way.nodes[i] + "," + way.nodes[i+1], false);
            wayAllNodesReserveMap.put(way.nodes[i+1] + "," + way.nodes[i], false);
        }
        for (Text value : values) {
            String[] nodes = value.toString().split(",");
            for (int i=0; i < nodes.length-1; i++) {
                if (wayAllNodesMap.get(nodes[i] + "," + nodes[i+1]) != null) {
                    wayAllNodesMap.put(nodes[i] + "," + nodes[i+1],true);
                }
                if (wayAllNodesReserveMap.get(nodes[i] + "," + nodes[i+1]) != null) {
                    wayAllNodesReserveMap.put(nodes[i] + "," + nodes[i+1],true);
                }
            }
        }
        int bingo = 0;
        for (Boolean ifBingo: wayAllNodesMap.values()) {
            if (ifBingo) {
                bingo ++;
            }
        }

        int bingoReserve = 0;
        for (Boolean ifBingo: wayAllNodesReserveMap.values()) {
            if (ifBingo) {
                bingoReserve ++;
            }
        }
        result.set(wayAllNodesMap.size() + Constant.RESULT_SEPARATOR + bingo + Constant.RESULT_SEPARATOR + bingoReserve);
        totalNodePair += wayAllNodesMap.size();
        bingoNodePair += bingo;
        totalNodePairReserve += wayAllNodesReserveMap.size();
        bingoNodePairReserve += bingoReserve;
        context.write(key, result);
    }

    @SuppressWarnings("unchecked")
    protected void cleanup(Context context) throws IOException, InterruptedException {
        System.out.println("total:" + totalNodePair);
        System.out.println("bingo:" + bingoNodePair);
        System.out.println("total reserve:" + totalNodePairReserve);
        System.out.println("bingo reserve:" + bingoNodePairReserve);
    }


    private static void loadOsmData() {
        osm = new OSM(null);
        osm.readFromFile("map/map.osm.pbf");
    }
}
