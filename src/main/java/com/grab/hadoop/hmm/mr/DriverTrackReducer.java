package com.grab.hadoop.hmm.mr;

/**
 * Created by hubo on 16/8/3.
 */

import com.grab.hadoop.envelope.mr.LongPair;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationParser;
import com.grab.hadoop.hmm.parser.MapMatchingResultAssembler;
import com.grab.hadoop.model.RedshiftDriverLocation;
import com.grab.speed.GrabGraphHopper;
import com.grab.speed.GrabMapMatchResult;
import com.grab.speed.GrabMapMatching;
import com.graphhopper.matching.LocationIndexMatch;
import com.graphhopper.matching.MapMatching;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.util.CmdArgs;
import com.graphhopper.util.GPXEntry;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DriverTrackReducer extends Reducer<LongPair, Text, LongWritable, Text> {
    private Text result = new Text();
    private LongWritable driverID = new LongWritable();

    private static GrabGraphHopper hopper;
    private static GraphHopperStorage graph;
    private static LocationIndexMatch locationIndex;
    private static MapMatching mapMatching;
    private static GrabMapMatching grabMapMatching;

    private static final int GPS_ACCURACY = 4;
    private static final int MAP_MATCH_WINDOW_NUM = 10;

    @SuppressWarnings("unchecked")
    protected void setup(Context context) throws IOException, InterruptedException {
        importOsmData();
        initialMatchingEngine();
    }

    @Override
    public void reduce(LongPair key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
        List<GPXEntry> toDoMapMathcing = new ArrayList<>();
        RedshiftDriverLocation lastLocation = new RedshiftDriverLocation();
        int stillLocation = 0;
        for (Text value : values) {
            RedshiftDriverLocation driverLocation = RedshiftDriverLocationParser.parseMeanSpeedJobMapResult(value.toString());
            if (driverLocation != null) {
                if (!isStill(lastLocation, driverLocation)) {
                    if (stillLocation > 0) {
                        GPXEntry entry = new GPXEntry(lastLocation.getLat(),lastLocation.getLon(),lastLocation.getLoggedTime().getTime());
                        toDoMapMathcing.add(entry);
                    }

                    GPXEntry entry = new GPXEntry(driverLocation.getLat(),driverLocation.getLon(),driverLocation.getLoggedTime().getTime());
                    toDoMapMathcing.add(entry);

                    if (toDoMapMathcing.size() >= MAP_MATCH_WINDOW_NUM) {
                        doMapMathcingAndWrite(toDoMapMathcing, context, driverLocation.getDriverID());
                        toDoMapMathcing.clear();
                    }

                    stillLocation = 0;
                } else {
                    stillLocation++;
                }
                lastLocation = driverLocation;
            }
        }
    }

    private void importOsmData() {
        // import OpenStreetMap data
        CmdArgs mockCmdArgs = new CmdArgs();
        mockCmdArgs.put("graph.flag_encoders","car");
        mockCmdArgs.put("datareader.file","map/map.osm.pbf");
        mockCmdArgs.put("prepare.min_network_size", 200);
        mockCmdArgs.put("prepare.min_one_way_network_size", 200);

        hopper = new GrabGraphHopper();
        hopper.init(mockCmdArgs);
        hopper.getCHFactoryDecorator().setEnabled(false);
        hopper.importOrLoad();

    }
    private void initialMatchingEngine() {
        FlagEncoder firstEncoder = hopper.getEncodingManager().fetchEdgeEncoders().get(0);
        graph = hopper.getGraphHopperStorage();

        locationIndex = new LocationIndexMatch(graph,(LocationIndexTree) hopper.getLocationIndex(), GPS_ACCURACY);

        mapMatching = new MapMatching(graph, locationIndex, firstEncoder);
        mapMatching.setMeasurementErrorSigma(GPS_ACCURACY);
    }

    private void doMapMathcingAndWrite(List<GPXEntry> list, Context context, Long driverId) {
        try {
            List<GrabMapMatchResult> matchResults = grabMapMatching.doGrabMapMatching(mapMatching,list);
            handleSnappedResult(context, driverId, matchResults);
        }catch (Exception ex) {
            for(int i=0; i < list.size() - 1; i++) {
                List<GPXEntry> sublist = list.subList(i,i+2);
                try {
                    List<GrabMapMatchResult> pairMatchResult = grabMapMatching.doGrabMapMatching(mapMatching,sublist);
                    handleSnappedResult(context, driverId, pairMatchResult);
                }catch (Exception e) {
                    System.out.println("-----broken-----");
                    for (GPXEntry entry :sublist){
                        System.out.println(entry.getLat() + "," + entry.getLon() + "," + entry.getTime());
                    }
                }
            }
        }
    }

    private void handleSnappedResult(Context context, Long driverId, List<GrabMapMatchResult> matchResults) throws IOException, InterruptedException {
        if (!matchResults.isEmpty()) {
            for (int i = 0; i < matchResults.size() - 1; i++) {
                int startWay = hopper.getInternalWayId(matchResults.get(i).getSnappedEdgeId());
                int endWay = hopper.getInternalWayId(matchResults.get(i+1).getSnappedEdgeId());

                GrabMapMatchResult start = matchResults.get(i);
                GrabMapMatchResult end = matchResults.get(i+1);

                if (startWay == endWay) {
                    String nodeString = getNodeSeriesString(startWay, matchResults.get(i).getSnappedLat(),matchResults.get(i).getSnappedLon(),matchResults.get(i+1).getSnappedLat(),matchResults.get(i+1).getSnappedLon());
                    if (!"empty".equals(nodeString)) {
                        String osmWayId = hopper.getOsmWayId(startWay) + "";
                        reduceOutput(context, driverId, osmWayId, start, end, nodeString);
                    }
                } else {
                    Long crossoverNodeID = hopper.findCrossNode(startWay,endWay);
                    if (crossoverNodeID != null) {
                        Double toCalculateLat,toCalculateLon;
                        Integer internalNodeId = hopper.getOsmNodeIdToInternalMap().get(crossoverNodeID);
                        if (internalNodeId == null) {
                            continue;
                        }
                        if (internalNodeId > 0) {
                            toCalculateLat = hopper.getPillarNodeLat(internalNodeId);
                            toCalculateLon = hopper.getPillarNodeLon(internalNodeId);
                        }else {
                            toCalculateLat = hopper.getGraphHopperStorage().getNodeAccess().getLat(-internalNodeId-3);
                            toCalculateLon = hopper.getGraphHopperStorage().getNodeAccess().getLon(-internalNodeId - 3);
                        }

                        List<Long> startNodes = hopper.getAdjacentNodeList(startWay, matchResults.get(i).getSnappedLat(),matchResults.get(i).getSnappedLon(),toCalculateLat,toCalculateLon);
                        List<Long> endNodes = hopper.getAdjacentNodeList(endWay, toCalculateLat,toCalculateLon, matchResults.get(i+1).getSnappedLat(),matchResults.get(i+1).getSnappedLon());
                        String nodes = getCrossNodeSeriesString(startNodes, endNodes);
                        if (!"empty".equals(nodes)) {
                            String osmWayIds = hopper.getOsmWayId(startWay) + "," + hopper.getOsmWayId(endWay);
                            reduceOutput(context, driverId, osmWayIds, start, end, nodes.toString());
                        }
                    }
                }
            }
        }
    }

    private void reduceOutput(Context context, Long driverId, String internalWayIds, GrabMapMatchResult start, GrabMapMatchResult end, String nodeString) throws IOException, InterruptedException {
        driverID.set(driverId);
        result.set(MapMatchingResultAssembler.assembleNodeToNodeResult(start, end, nodeString, internalWayIds));
        context.write(driverID, result);
    }

    private String getNodeSeriesString(int internalWay, double startLat, double startLon, double endLat, double endLon) {
        StringBuilder nodesBuilder = new StringBuilder();
        List<Long> nodes = hopper.getAdjacentNodeList(internalWay, startLat, startLon, endLat, endLon);

        if (!nodes.isEmpty()) {
            for (long nodeId : nodes) {
                nodesBuilder.append(nodeId).append(",");
            }
        }

        return nodesBuilder.toString().length() > 0 ? nodesBuilder.toString().substring(0, nodesBuilder.length() - 1) : "empty";
    }

    private String getCrossNodeSeriesString(List<Long> start, List<Long> end) {
        StringBuilder nodesBuilder = new StringBuilder();
        Set<Long> startSet = new HashSet<>(start.size());
        if (!(!start.isEmpty() && start.size()==2 && (start.get(0).longValue() == start.get(1).longValue()))) {
            for (long nodeId : start) {
                nodesBuilder.append(nodeId).append(",");
                startSet.add(nodeId);
            }
        }
        if (!(!end.isEmpty() && end.size()==2 && (end.get(0).longValue() == end.get(1).longValue()))) {
            for (long nodeId : end) {
                if (!startSet.contains(nodeId)) {
                    nodesBuilder.append(nodeId).append(",");
                }
            }
        }
        return nodesBuilder.toString().length() > 0 ? nodesBuilder.toString().substring(0, nodesBuilder.length() - 1) : "empty";
    }

    private boolean isStill(RedshiftDriverLocation first, RedshiftDriverLocation second) {
        return first != null && second != null && first.getDriverID() == second.getDriverID() && first.getLat() == second.getLat() && first.getLon() == second.getLon();
    }
}
