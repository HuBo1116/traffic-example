package com.grab.hadoop.hmm.mr;

import com.grab.hadoop.hmm.parser.AvgSpeedParser;
import com.grab.hadoop.model.MeanSpeed;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hubo on 16/8/12.
 */
public class WayCoverageMapper extends Mapper<LongWritable, Text, LongWritable, Text> {
    private LongWritable wayId = new LongWritable();
    private Text nodes = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        MeanSpeed meanSpeed = AvgSpeedParser.parseHmmSpeedJobResult(value.toString());
        if (meanSpeed !=null) {
            wayId.set(meanSpeed.getWayID());
            nodes.set(meanSpeed.getNodeSet());
            context.write(wayId, nodes);
        }
    }
}
