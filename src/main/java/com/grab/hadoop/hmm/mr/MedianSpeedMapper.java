package com.grab.hadoop.hmm.mr;

import com.grab.hadoop.hmm.parser.AvgSpeedParser;
import com.grab.hadoop.model.MeanSpeed;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hubo on 16/8/11.
 */
public class MedianSpeedMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {
    private Text nodeToNodeInfo = new Text();
    private DoubleWritable speed = new DoubleWritable();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        MeanSpeed meanSpeed = AvgSpeedParser.parseHmmSpeedJobResult(value.toString());
        if (meanSpeed !=null) {
            String[] nodes = meanSpeed.getNodeSet().split(",");
            if (nodes.length >= 2) {
                for (int i=1; i < nodes.length; i++) {
                    nodeToNodeInfo.set(nodes[i-1] + "," + nodes[i]);
//                    nodeToNodeInfo.set(nodes[i-1] + "," + nodes[i] +  Constant.RESULT_SEPARATOR + meanSpeed.getHourOfWeek());
//                    nodeToNodeInfo.set(meanSpeed.getWayID()+","+nodes[i-1] + "," + nodes[i]); //without hour
                    speed.set(meanSpeed.getAvgSpeed());
                    context.write(nodeToNodeInfo, speed);
                }
            }
        }
    }
}
