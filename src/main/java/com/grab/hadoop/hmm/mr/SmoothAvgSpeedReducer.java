package com.grab.hadoop.hmm.mr;

import cern.colt.list.DoubleArrayList;
import cern.jet.stat.Descriptive;
import com.conveyal.osmlib.Node;
import com.conveyal.osmlib.OSM;
import com.conveyal.osmlib.Way;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.DistancePlaneProjection;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by hubo on 16/8/11.
 */
public class SmoothAvgSpeedReducer extends Reducer<LongWritable, Text, Text, Text> {
    private Text nodePair = new Text();
    private Text result = new Text();
    private OSM osm;
    private Set<String> trafficSignal = new HashSet<>();
    private int crossCount;
    private static DistanceCalc distanceCalc = new DistancePlaneProjection();

    @SuppressWarnings("unchecked")
    protected void setup(Context context) throws IOException, InterruptedException {
        osm = new OSM(null);
        osm.readFromFile("map/map.osm.pbf");
    }

    @Override
    public void reduce(LongWritable key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
        Way way = osm.ways.get(key.get());
        smoothWaySpeed(values, context, way);
    }

    private void smoothWaySpeed(Iterable<Text> values, Context context, Way way) throws IOException, InterruptedException {
        Map<String,List<Double>> nodePairSpeedListMap = new HashMap<>();

        Map<String,Integer> forwardNodePairToIndexMapping = new HashMap<>();
        Map<String,Integer> backwardNodePairToIndexMapping = new HashMap<>();

        Map<Integer,String> forwardIndexToNodePairMapping = new HashMap<>();
        Map<Integer,String> backwardIndexToNodePairMapping = new HashMap<>();

        double[] forwardNodePairAvgSpeed = new double[way.nodes.length-1];
        double[] backwardNodePairAvgSpeed = new double[way.nodes.length-1];

        for (int i=0; i < way.nodes.length-1; i++) {
            forwardNodePairToIndexMapping.put(way.nodes[i] + "," + way.nodes[i+1], i);
            forwardIndexToNodePairMapping.put(i, way.nodes[i] + "," + way.nodes[i+1]);

            backwardNodePairToIndexMapping.put(way.nodes[i+1] + "," + way.nodes[i], way.nodes.length-2-i);
            backwardIndexToNodePairMapping.put(way.nodes.length-2-i, way.nodes[i+1] + "," + way.nodes[i]);

            if (isTrafficSignal(way.nodes[i])) {
                trafficSignal.add(way.nodes[i] + "," + way.nodes[i+1]);
                trafficSignal.add(way.nodes[i+1] + "," + way.nodes[i]);
                if (i != 0 ) {
                    trafficSignal.add(way.nodes[i] + "," + way.nodes[i-1]);
                    trafficSignal.add(way.nodes[i-1] + "," + way.nodes[i]);
                }
            }

            if (isTrafficSignal(way.nodes[i+1])) {
                trafficSignal.add(way.nodes[i] + "," + way.nodes[i+1]);
                trafficSignal.add(way.nodes[i+1] + "," + way.nodes[i]);
            }
        }

        for (Text value : values) {
            String[] nodesAndSpeed = value.toString().split(";");
            if (nodesAndSpeed.length != 2) {
                continue;
            }
            String[] nodes = nodesAndSpeed[0].split(",");
            for (int i=0; i < nodes.length - 1; i++) {
                String nodePair = nodes[i] + "," + nodes[i+1];
                List<Double> speedList =  nodePairSpeedListMap.get(nodePair);
                if (speedList == null) {
                    speedList = new ArrayList<>();
                }
                speedList.add(Double.parseDouble(nodesAndSpeed[1]));
                nodePairSpeedListMap.put(nodePair,speedList);
            }
        }

        for (String nodePair : nodePairSpeedListMap.keySet()) {
            List<Double> speedList = nodePairSpeedListMap.get(nodePair);

            if (forwardNodePairToIndexMapping.get(nodePair) != null) {
                Integer index = forwardNodePairToIndexMapping.get(nodePair);
                forwardNodePairAvgSpeed[index] = calculateAvgSpeed(speedList);
            }

            if (backwardNodePairToIndexMapping.get(nodePair) != null) {
                Integer index = backwardNodePairToIndexMapping.get(nodePair);
                backwardNodePairAvgSpeed[index] = calculateAvgSpeed(speedList);
            }
        }
        doSmooth(forwardNodePairAvgSpeed,forwardIndexToNodePairMapping,context);
        doSmooth(backwardNodePairAvgSpeed,backwardIndexToNodePairMapping,context);
    }



    private  void doSmooth(double[] nodePairAvgSpeed, Map<Integer,String> indexToNodePairMapping, Context context) throws IOException, InterruptedException {
        Map<Integer,Double> additional = smoothFilling(nodePairAvgSpeed);
        for (Integer index : additional.keySet()) {
            String additionalNodePair = indexToNodePairMapping.get(index);
            Double additionalSpeed;
            if (trafficSignal.contains(additionalNodePair)) {
                additionalSpeed = additional.get(index)/2;
                crossCount++;
            } else {
                additionalSpeed = additional.get(index);
            }

            nodePair.set(additionalNodePair);
            result.set(new BigDecimal(additionalSpeed).setScale(0,BigDecimal.ROUND_UP).intValue() + "");
            context.write(nodePair, result);
        }
    }

    private  double calculateAvgSpeed(List<Double> speedList) {
        double[] speedArr = new double[speedList.size()];
        for (int i = 0; i < speedList.size(); i++) {
            speedArr[i] = speedList.get(i);
        }
        DoubleArrayList doubleSpeedArraylist = new DoubleArrayList(speedArr);

        return Descriptive.median(doubleSpeedArraylist);
    }

    public static Map<Integer,Double> smoothFilling(double[] list) {
        Map<Integer,Double> additionalMap = new HashMap<>();
        int lastNotEmptyIndex = -1;
        int firstNotEmptyIndex = -1;
        for (int i=0; i < list.length; i++) {
            if (list[i] != 0) {
                if (lastNotEmptyIndex == -1) {
                    firstNotEmptyIndex = i;
                    lastNotEmptyIndex = i;
                    continue;
                } else {
                    double stepValue = Math.abs((list[i] - list[lastNotEmptyIndex])/(i - lastNotEmptyIndex));
                    for (int j=lastNotEmptyIndex+1; j < i; j++) {
                        list[j] = list[j-1] + stepValue;
                        if (list[i] > list[lastNotEmptyIndex]) {
                            list[j] = list[j-1] + stepValue;
                            additionalMap.put(j, list[j-1] + stepValue);
                        }else {
                            list[j] = list[j-1] - stepValue;
                            additionalMap.put(j, list[j-1] - stepValue);
                        }
                    }
                    lastNotEmptyIndex = i;
                }
            }
        }
        if (firstNotEmptyIndex != -1 && lastNotEmptyIndex != -1 ) {
            for (int i=0; i < firstNotEmptyIndex; i++) {
                list[i] = list[firstNotEmptyIndex];
                additionalMap.put(i, list[firstNotEmptyIndex]);
            }
            for (int i=lastNotEmptyIndex+1; i < list.length; i++) {
                list[i] = list[lastNotEmptyIndex];
                additionalMap.put(i, list[lastNotEmptyIndex]);
            }
        }
        return additionalMap;
    }

    private boolean isTrafficSignal(long nodeId) {
        Node node = osm.nodes.get(nodeId);
        String tag = node.getTag("highway");
        if (tag != null && ("traffic_signals".equals(tag) || "crossing".equals(tag))) {
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    protected void cleanup(Context context) throws IOException, InterruptedException {
        for (String nodePair: trafficSignal) {
            String[] node = nodePair.split(",");
            Node start = osm.nodes.get(Long.parseLong(node[0]));
            Node end = osm.nodes.get(Long.parseLong(node[1]));
            System.out.println(nodePair + ", distance:" + distanceCalc.calcDist(start.getLat(),start.getLon(),end.getLat(),end.getLon()));
        }
        System.out.println("cross:" + crossCount);
    }
}
