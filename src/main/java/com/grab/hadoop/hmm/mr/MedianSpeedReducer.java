package com.grab.hadoop.hmm.mr;

import cern.colt.list.DoubleArrayList;
import cern.jet.stat.Descriptive;
import com.grab.hadoop.common.Constant;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hubo on 16/8/11.
 */
public class MedianSpeedReducer extends Reducer<Text, DoubleWritable, Text, Text> {
    private Text result = new Text();
    private static int VALID_NUM = 4;
    private static double CORRECT_FACTOR= 1.25;

    @Override
    public void reduce(Text key, Iterable<DoubleWritable> values,Context context) throws IOException, InterruptedException {
        List<Double> speedList = new ArrayList<>();
        for (DoubleWritable speed : values) {
            speedList.add(speed.get());
        }
        double[] speedArr = new double[speedList.size()];
        for (int i = 0; i < speedList.size(); i++) {
            speedArr[i] = speedList.get(i);
        }
        DoubleArrayList doubleSpeedArraylist = new DoubleArrayList(speedArr);
        if (speedList.size() >= VALID_NUM) {
            double originSpeed = Descriptive.mean(doubleSpeedArraylist);
            int meanSpeed = new BigDecimal(originSpeed).setScale(0,BigDecimal.ROUND_HALF_UP).intValue();
            int correctedSpeed = new BigDecimal(originSpeed).divide(new BigDecimal(CORRECT_FACTOR),0,BigDecimal.ROUND_HALF_UP).intValue();
            result.set(meanSpeed + Constant.RESULT_SEPARATOR + correctedSpeed);
            context.write(key, result);
        }
        context.write(key, result);
    }

}
