package com.grab.hadoop.hmm.mr;

import com.grab.hadoop.hmm.parser.AvgSpeedParser;
import com.grab.hadoop.model.MeanSpeed;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hubo on 16/8/11.
 */
public class SmoothAvgSpeedMapper extends Mapper<LongWritable, Text, LongWritable, Text> {
    private LongWritable wayID = new LongWritable();
    private Text result = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        MeanSpeed meanSpeed = AvgSpeedParser.parseHmmSpeedJobResult(value.toString());
        if (meanSpeed !=null) {
            String[] nodes = meanSpeed.getNodeSet().split(",");
            if (nodes.length >= 2) {
                for (int i=1; i < nodes.length; i++) {
                    wayID.set(meanSpeed.getWayID());
                    result.set(meanSpeed.getNodeSet() + ";" + meanSpeed.getAvgSpeed());
                    context.write(wayID, result);
                }
            }
        }
    }
}
