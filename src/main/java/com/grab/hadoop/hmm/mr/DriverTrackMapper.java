package com.grab.hadoop.hmm.mr;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.envelope.mr.LongPair;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationParser;
import com.grab.hadoop.envelope.parser.RedshiftDriverLocationResultAssembler;
import com.grab.hadoop.model.RedshiftDriverLocation;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by hubo on 16/5/31.
 */
public class DriverTrackMapper extends Mapper<LongWritable, Text, LongPair, Text> {
    private LongPair driverTimestampKey = new LongPair();
    private Text locationInfo = new Text();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        RedshiftDriverLocation driverLocation = RedshiftDriverLocationParser.parseRedshiftETAData(value.toString());
        if (driverLocation != null) {
            driverLocation.setDriverID(driverLocation.getDriverID());
            if (driverLocation.getAccuracy() <= Constant.VALID_ACCURACY_THRESHOLD) {
                driverTimestampKey.set(driverLocation.getDriverID(), driverLocation.getLoggedTime().getTime());
                locationInfo.set(RedshiftDriverLocationResultAssembler.assembleSpeedMapResult(driverLocation));
                context.write(driverTimestampKey, locationInfo);
            }
        }
    }
}
