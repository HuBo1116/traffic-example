package com.grab.hadoop.hmm;

import com.grab.hadoop.hmm.mr.SmoothAvgSpeedMapper;
import com.grab.hadoop.hmm.mr.SmoothAvgSpeedReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Created by hubo on 16/8/11.
 */
public class SmoothSpeedJob {
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: MaxTemperature <input path> <output path>");
            System.exit(-1);
        }

        Configuration conf = new Configuration();
        Job avgSpeedJob = new Job(conf,"smooth speed");

        avgSpeedJob.setMapperClass(SmoothAvgSpeedMapper.class);
        avgSpeedJob.setReducerClass(SmoothAvgSpeedReducer.class);

        avgSpeedJob.setMapOutputKeyClass(LongWritable.class);
        avgSpeedJob.setMapOutputValueClass(Text.class);

        avgSpeedJob.setOutputKeyClass(Text.class);
        avgSpeedJob.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(avgSpeedJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(avgSpeedJob, new Path(args[1]));

        avgSpeedJob.waitForCompletion(true);
    }
}
