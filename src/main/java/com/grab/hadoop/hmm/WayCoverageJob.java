package com.grab.hadoop.hmm;

import com.grab.hadoop.hmm.mr.WayCoverageMapper;
import com.grab.hadoop.hmm.mr.WayCoverageReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Created by hubo on 16/8/13.
 */
public class WayCoverageJob {
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: MaxTemperature <input path> <output path>");
            System.exit(-1);
        }

        Configuration conf = new Configuration();
        Job wanCoverageJob = new Job(conf,"grab way");

        wanCoverageJob.setMapperClass(WayCoverageMapper.class);
        wanCoverageJob.setReducerClass(WayCoverageReducer.class);

        wanCoverageJob.setMapOutputKeyClass(LongWritable.class);
        wanCoverageJob.setMapOutputValueClass(Text.class);

        wanCoverageJob.setOutputKeyClass(LongWritable.class);
        wanCoverageJob.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(wanCoverageJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(wanCoverageJob, new Path(args[1]));

        wanCoverageJob.waitForCompletion(true);
    }
}
