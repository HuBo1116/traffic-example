package com.grab.hadoop.hmm.parser;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.MeanSpeed;

/**
 * Created by hubo on 16/8/11.
 */
public class AvgSpeedParser {
    public static MeanSpeed parseHmmSpeedJobResult(String item){
        MeanSpeed meanSpeed = new MeanSpeed();
        try{
            String[] fields = item.split(Constant.RESULT_SEPARATOR);
            if(fields.length == Constant.HMM_SPEED_JOB_RESULT_LENGTH){
                meanSpeed.setDriverID(Long.parseLong(fields[0]));
                meanSpeed.setNodeSet(fields[5]);
                meanSpeed.setDistance(Double.parseDouble(fields[6]));
                meanSpeed.setAvgSpeed(Double.parseDouble(fields[7]));
                meanSpeed.setHourOfWeek((Integer.parseInt(fields[8]) - 1) * 24 + Integer.parseInt(fields[9]));
                meanSpeed.setWayID(Long.parseLong(fields[12]));
            }else{
                meanSpeed = null;
            }
        }catch(Exception e){
            meanSpeed = null;
        }
        return meanSpeed;
    }
}
