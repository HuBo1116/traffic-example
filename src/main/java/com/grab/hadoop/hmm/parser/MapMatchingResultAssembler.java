package com.grab.hadoop.hmm.parser;

import com.grab.hadoop.common.Constant;
import com.grab.hadoop.model.MapMatchingResult;
import com.grab.speed.GrabMapMatchResult;
import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.DistancePlaneProjection;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


/**
 * Created by hubo on 16/6/1.
 */
public class MapMatchingResultAssembler {
    private static DistanceCalc distanceCalc = new DistancePlaneProjection();
    private static Calendar calendar = Calendar.getInstance();

    public static String assembleMapMathcingResult(MapMatchingResult mapMathcingResult){
        StringBuilder sb = new StringBuilder();
        sb.append(mapMathcingResult.getOriginCoordinates()).append(Constant.RESULT_SEPARATOR);
        sb.append(mapMathcingResult.getSnappedCoordinates()).append(Constant.RESULT_SEPARATOR);
        sb.append(mapMathcingResult.getNodeToNodeList());
        return sb.toString();
    }

    public static String assembleNodeToNodeResult(GrabMapMatchResult start, GrabMapMatchResult end, String nodes, String osmWayId){
        StringBuilder sb = new StringBuilder();
        sb.append(start.getOriginCoordinate()).append(Constant.RESULT_SEPARATOR);
        sb.append(end.getOriginCoordinate()).append(Constant.RESULT_SEPARATOR);
        sb.append(start.getTime()).append(Constant.RESULT_SEPARATOR);
        sb.append(end.getTime()).append(Constant.RESULT_SEPARATOR);
        sb.append(nodes).append(Constant.RESULT_SEPARATOR);
        double distance = distanceCalc.calcDist(start.getSnappedLat(),start.getSnappedLon(),end.getSnappedLat(),end.getSnappedLon());
        sb.append(distance).append(Constant.RESULT_SEPARATOR);
        if (start.getTime() < end.getTime()) {
            double speed = distance * 1000 * 3.6 / (end.getTime() - start.getTime());
            sb.append(speed).append(Constant.RESULT_SEPARATOR);
        }else {
            sb.append(-1).append(Constant.RESULT_SEPARATOR);
        }

        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        calendar.setTime(new Date(start.getTime()));
        sb.append(calendar.get(Calendar.DAY_OF_WEEK)).append(Constant.RESULT_SEPARATOR);
        sb.append(calendar.get(Calendar.HOUR_OF_DAY)).append(Constant.RESULT_SEPARATOR);
        sb.append(start.getSnappedCoordinate()).append(Constant.RESULT_SEPARATOR);
        sb.append(end.getSnappedCoordinate()).append(Constant.RESULT_SEPARATOR);
        sb.append(osmWayId);
        return sb.toString();
    }
}
