package com.grab.hadoop.hmm;

import com.grab.hadoop.envelope.mr.DriverIDPartitioner;
import com.grab.hadoop.envelope.mr.GroupingComparator;
import com.grab.hadoop.envelope.mr.LongPair;
import com.grab.hadoop.hmm.mr.DriverTrackMapper;
import com.grab.hadoop.hmm.mr.DriverTrackReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Created by hubo on 16/6/1.
 */
public class HmmPreJob {
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: MaxTemperature <input path> <output path>");
            System.exit(-1);
        }

        Configuration conf = new Configuration();
        Job mapMatchingPreJob = new Job(conf,"grab speed");

        mapMatchingPreJob.setMapperClass(DriverTrackMapper.class);
        mapMatchingPreJob.setReducerClass(DriverTrackReducer.class);

        mapMatchingPreJob.setPartitionerClass(DriverIDPartitioner.class);
        mapMatchingPreJob.setGroupingComparatorClass(GroupingComparator.class);

        mapMatchingPreJob.setMapOutputKeyClass(LongPair.class);
        mapMatchingPreJob.setMapOutputValueClass(Text.class);

        mapMatchingPreJob.setOutputKeyClass(LongWritable.class);
        mapMatchingPreJob.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(mapMatchingPreJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(mapMatchingPreJob, new Path(args[1]));

        mapMatchingPreJob.waitForCompletion(true);
    }
}
