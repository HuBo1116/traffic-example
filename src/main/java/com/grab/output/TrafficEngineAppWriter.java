package com.grab.output;

import com.grab.app.TrafficUpdate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by hubo on 16/3/23.
 */
@Deprecated
public class TrafficEngineAppWriter extends Thread {
    private long nextUpdateTimeInMillis;
    private static final Log Logger = LogFactory.getLog(OsrmWriter.class);
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);

    private static GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Asia/Shanghai"));
    private static long intervarlMillis = Long.parseLong(TrafficUpdate.appProps.getProperty("application.update.app.interval")) * 60 * 1000;

    public TrafficEngineAppWriter(long nextUpdateTimeInMillis) {
        this.nextUpdateTimeInMillis = nextUpdateTimeInMillis;
    }


    @Override
    public void run() {
        while (true) {
            // If it is time to report stats as per the reporting interval, report stats
            long checkUpdate = System.currentTimeMillis();
            if (checkUpdate > nextUpdateTimeInMillis) {
                try {
                    printTrafficEngineAppCsv(System.currentTimeMillis());
                } catch (FileNotFoundException e) {
                    Logger.error("Can't update csv:",e);
                }
                nextUpdateTimeInMillis = checkUpdate + intervarlMillis;
            }
        }
    }

    private void printTrafficEngineAppCsv(long timestamp) throws FileNotFoundException {
        if (TrafficUpdate.location != null && TrafficUpdate.location.set.size() > 0) {
            calendar.setTimeInMillis(timestamp);

            PrintWriter printWriter = new PrintWriter(TrafficUpdate.appProps.getProperty("application.update.app.dir") + "/update_app_" + sdf.format(calendar.getTime()) + ".csv");
            for (String locationLine : TrafficUpdate.location.set) {
                printWriter.println(locationLine);
            }
            printWriter.flush();
            printWriter.close();
            TrafficUpdate.location.clearAllData();
        }
    }
}
