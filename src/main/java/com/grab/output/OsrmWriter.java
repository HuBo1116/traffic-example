package com.grab.output;

import com.grab.app.TrafficUpdate;
import com.grab.data.TrafficCondition;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mapdb.Fun;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by hubo on 16/3/14.
 */
public class OsrmWriter extends Thread {

    private long nextUpdateTimeInMillis;
    private static final Log Logger = LogFactory.getLog(OsrmWriter.class);
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);

    private static GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Asia/Shanghai"));
    private static long intervarlMillis = Long.parseLong(TrafficUpdate.appProps.getProperty("application.update.osrm.interval")) * 60 * 1000;//millisecond

    public OsrmWriter(long nextUpdateTimeInMillis) {
        this.nextUpdateTimeInMillis = nextUpdateTimeInMillis;
    }


    @Override
    public void run() {
        while (true) {
            // If it is time to report stats as per the reporting interval, report stats
            long checkUpdate = System.currentTimeMillis();
            if (checkUpdate > nextUpdateTimeInMillis) {
                try {
                    long currentTime = System.currentTimeMillis();
                    printOsrmTrafficCsv(currentTime, TrafficCondition.CAR_TYPE);
                    printOsrmTrafficCsv(currentTime, TrafficCondition.BIKE_TYPE);
                } catch (FileNotFoundException e) {
                    Logger.error("Can't update csv:",e);
                }
                nextUpdateTimeInMillis = checkUpdate + intervarlMillis;
            }
        }
    }

    private void printOsrmTrafficCsv(long timestamp, int vehicleType) throws FileNotFoundException {
        if (TrafficUpdate.trafficCondition != null) {
            calendar.setTimeInMillis(timestamp);
            ConcurrentNavigableMap<Fun.Tuple3<String,Integer,Integer>,Integer> map = null;
            String fileName = TrafficUpdate.appProps.getProperty("application.update.dir");
            if(vehicleType == TrafficCondition.CAR_TYPE) {
                map = TrafficUpdate.trafficCondition.carMap;
                fileName += "/car_update_" +sdf.format(calendar.getTime())+".csv";
            }else if(vehicleType == TrafficCondition.BIKE_TYPE) {
                map = TrafficUpdate.trafficCondition.bikeMap;
                fileName += "/bike_update_" +sdf.format(calendar.getTime())+".csv";
            }else {
                return;
            }

            PrintWriter printWriter = new PrintWriter(fileName);
            String curWayId = null;
            for(Map.Entry<Fun.Tuple3<String, Integer, Integer>, Integer> entry : map.entrySet()){
                Fun.Tuple3<String, Integer, Integer> key = entry.getKey();
                String wayId = key.a;
                String[] item = wayId.split("\\|");
                if (item.length != 3) {
                    continue;
                }
                if(!wayId.equals(curWayId)){
                    Integer meanSpeed = TrafficUpdate.trafficCondition.meanSpeed(wayId, vehicleType);
                    if (meanSpeed == null) {
                        continue;
                    }
                    printWriter.println(item[1]+","+item[2]+","+ meanSpeed);
                    printWriter.flush();

                    curWayId = wayId;
                }
            }
            printWriter.close();
            TrafficUpdate.trafficCondition.clearAllData(vehicleType);
        }
    }
}
