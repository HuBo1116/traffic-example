package com.grab.playground.jts;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.operation.buffer.BufferOp;

public class Buffer {

    public static void main(String[] args) {
        Coordinate[] coords = new Coordinate[2];
        coords[0] = new Coordinate(1, 1);
        coords[1] = new Coordinate(2, 2);

        GeometryFactory geometryFactory = new GeometryFactory();
        Geometry lineString = geometryFactory.createLineString(coords);

        BufferOp bufOp = new BufferOp(lineString);
        bufOp.setEndCapStyle(BufferOp.CAP_ROUND);
        Geometry bg = bufOp.getResultGeometry(0.5);
        System.out.println(bg);
    }
}
