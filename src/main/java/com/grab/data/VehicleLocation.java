package com.grab.data;


import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by hubo on 16/3/21.
 */
public class VehicleLocation {

    public Set<String> set;

    public VehicleLocation(){
        set = new ConcurrentSkipListSet();
    }

    public void add(String vehicleLocation) {
        set.add(vehicleLocation);
    }

    public void clearAllData() {
        set.clear();
    }
}