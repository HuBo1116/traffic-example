-- Enable PostGIS (includes raster)
CREATE EXTENSION postgis;
-- Enable Topology
CREATE EXTENSION postgis_topology;

DROP TABLE IF EXISTS pending_track;
CREATE TABLE pending_track
(
  id                      SERIAL PRIMARY KEY,
  find_time               TIMESTAMP WITH TIME ZONE NOT NULL,
  driver_id               INTEGER NOT NULL,
  original_track          GEOGRAPHY (LINESTRING ,4326) NOT NULL,
  start_barrier           GEOGRAPHY(LINESTRING,4326) NOT NULL,
  end_barrier             GEOGRAPHY(LINESTRING,4326) NOT NULL,
  start_way_id            INTEGER NOT NULL,
  end_way_id              INTEGER NOT NULL,
  optimized_track         GEOGRAPHY(LINESTRING,4326),
  state                   INTEGER NOT NULL DEFAULT 0
);
