package com.grab.app;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.conveyal.osmlib.OSM;
import com.grab.data.VehicleLocation;
import com.grab.engine.enrich.EnrichEngine;
import com.grab.engine.enrich.EnrichEngineExecuter;
import com.grab.external.controlcenter.VehicleType;
import com.grab.external.controlcenter.VehicleTypeFilter;
import com.grab.kinesis.model.Location;
import com.grab.kinesis.processor.LocationEnrichProcessorFactory;
import com.grab.kinesis.util.ConfigurationUtils;
import com.grab.kinesis.util.CredentialUtils;
import com.grab.statsd.StatsD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.postgresql.jdbc3.Jdbc3PoolingDataSource;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by hubo on 16/4/20.
 */
public class EnrichMap {
    private static final Log Logger = LogFactory.getLog(TrafficUpdate.class);
    private static final String CONFIG_FILE_PATH="application.conf";
    public static Properties appProps = new Properties();
    public static OSM osm;
    public static VehicleLocation location;
    public static EnrichEngineExecuter enrichEngineExecuter;
    public static StatsD statsD;
    public static BlockingQueue<Location> locationQueue;
    public static EnrichEngine enrichEngine;
    public static Map<Long,String> cityCodeMap;
    public static Jdbc3PoolingDataSource source;

    public static void main(String[] args) throws Exception {
        // load settings file
        loadSettings(CONFIG_FILE_PATH);

        loadCityCodeMap();

        startOSM();

        Integer numberOfWorkerCores = startEnrichEngine();
        startStatD(numberOfWorkerCores);

        startPostgis();

        startEnrichEngine();

        startKinesis();
    }

    public static void startPostgis(){
        source = new Jdbc3PoolingDataSource();
        source.setDataSourceName("Pending Track");
        source.setServerName(appProps.getProperty("application.postgis.host"));
        source.setPortNumber(Integer.parseInt(appProps.getProperty("application.postgis.port")));
        source.setDatabaseName(appProps.getProperty("application.postgis.database"));
        source.setUser(appProps.getProperty("application.postgis.user"));
        source.setPassword(appProps.getProperty("application.postgis.passport"));
        source.setMaxConnections(20);
    }

    private static void startOSM() {
        Logger.info("======== OSM start ======== ");
        osm = new OSM(null);
        osm.readFromFile(appProps.getProperty("application.osm.dir"));
        Logger.info("======== OSM end ======== ");
    }

    private static Integer startEnrichEngine() {
        Logger.info("======== ENRICH ENGINE start ======== ");
        enrichEngine = new EnrichEngine();
        enrichEngine.addProbeBarrier(osm);

        enrichEngineExecuter = new EnrichEngineExecuter();

        Integer numberOfWorkerCores;
        try {
            numberOfWorkerCores = Integer.parseInt(appProps.getProperty("application.engine.worker.num"));
        } catch(Exception e) {
            numberOfWorkerCores = Runtime.getRuntime().availableProcessors() / 2;
            Logger.info("Property numberOfWorkerCores not set, defaulting to " + numberOfWorkerCores + " cores.");

        }

        Integer queueLimit = Integer.parseInt(appProps.getProperty("application.engine.queue.limit"));
        locationQueue = new ArrayBlockingQueue<>(queueLimit);
        enrichEngineExecuter.start(numberOfWorkerCores,locationQueue);

        EnrichEngine.batchScheduleOptimizeTrack();

        Logger.info("======== ENRICH ENGINE end ======== ");

        return numberOfWorkerCores;
    }

    private static void startKinesis() throws Exception {
        Logger.info("========Kinesis start ======== ");

        Region region = RegionUtils.getRegion(appProps.getProperty("application.kinesis.region"));
        if (region == null) {
            System.err.println("ap-southeast-1 is not a valid AWS region.");
            System.exit(1);
        }

        String workerId = String.valueOf(UUID.randomUUID());

        AWSCredentialsProvider credentialsProvider = CredentialUtils.getCredentialsProvider();

        KinesisClientLibConfiguration kclConfig =
                new KinesisClientLibConfiguration(ConfigurationUtils.getClientConfigWithUserAgent().getUserAgent(), appProps.getProperty("application.kinesis.stream.name"), credentialsProvider, workerId)
                        .withRegionName(region.getName())
                        .withCommonClientConfig(ConfigurationUtils.getClientConfigWithUserAgent())
                        .withMaxRecords(Integer.parseInt(appProps.getProperty("applicaiton.kinesis.max.records")))
                        .withIdleTimeBetweenReadsInMillis(Long.parseLong(appProps.getProperty("application.kinesis.read.interval")));


        IRecordProcessorFactory recordProcessorFactory = new LocationEnrichProcessorFactory();

        // Create the KCL worker with the location record processor factory
        Worker worker = new Worker(recordProcessorFactory, kclConfig);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            Logger.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        System.exit(exitCode);
        Logger.info("========Kinesis end ======== ");
    }

    public static void loadSettings(String configFilePath) {
        if(configFilePath == null)
            configFilePath = "application.conf";
        try {
            FileInputStream in = new FileInputStream(configFilePath);
            appProps.load(in);
            in.close();

            File updatePath = new File(appProps.getProperty("application.update.dir"));
            updatePath.mkdirs();

            File updateAppPath = new File(appProps.getProperty("application.update.app.dir"));
            updateAppPath.mkdirs();

        } catch (IOException e) {
            System.err.println("Unable to load application.conf file.");
            System.exit(1);
        }
    }

    private static void startStatD(Integer numberOfWorkerCores) {
        Logger.info("========StatD start ======== ");
        statsD  = new StatsD();
        statsD.initiate(numberOfWorkerCores);
        Logger.info("========StatD end ======== ");
    }

    public static void loadCityCodeMap(){
        VehicleTypeFilter filter = new VehicleTypeFilter();
        try {
            List<VehicleType> list = filter.readVehicleDataFromLocalFile();
            cityCodeMap = new HashMap<>(list.size());
            for(VehicleType vehicleType:list){
                cityCodeMap.put(vehicleType.getId(),vehicleType.getCityCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
