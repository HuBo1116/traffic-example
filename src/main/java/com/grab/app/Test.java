package com.grab.app;

import cern.colt.list.DoubleArrayList;
import cern.jet.stat.Descriptive;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hubo on 16/8/12.
 */
public class Test {
    public static void main(String[] args) throws Exception {
        double[] t = new double[22];
//        t[0] = 3.000337679847087;
//        t[1] = 0.6634150135269852;
//        t[2] = 5.084917351048096;


//        t[0] = 24.394297592264532;
//        t[1] = 18.11561144657504;
//        t[2] = 14.67742816590545;
//        t[3] = 7.40408477557662;
//        t[4] = 9.47700856129578;


        t[0] = 6.290815132150735;
        t[1] = 19.884679637105105;
        t[2] = 3.912285607067204;
        t[3] = 21.920245916472297;
        t[4] = 15.302425007227788;
        t[5] = 32.9534388263048;
        t[6] = 20.173587672957787;
        t[7] = 14.044751404440518;
        t[8] = 5.7245522660225046;
        t[9] = 8.873811573713954;
        t[10] = 17.242910607768078;
        t[11] = 13.8531213804195;
        t[12] = 33.49994186765467;
        t[13] = 29.878611224232994;
        t[14] = 30.70612450885062;
        t[15] = 8.745658231777869;
        t[16] = 24.89900954385885;
        t[17] = 15.627984370158277;
        t[18] = 47.08824255015593;
        t[19] = 10.119733021914655;
        t[20] = 0.12346670833699347;
        t[21] = 26.3793545172397;

//        DoubleArrayList doubleSpeedArraylist = new DoubleArrayList(t);
//        System.out.println(Descriptive.median(doubleSpeedArraylist));
//        System.out.println(new BigDecimal(Descriptive.median(doubleSpeedArraylist)).setScale(0,BigDecimal.ROUND_HALF_UP).intValue());
//
//
//        double[] test = new double[10];
//        test[3] = 30;
//        test[4] = 40;
//        test[7] = 70;
//        Map<Integer,Double> result = Test.smoothFilling(test);
//        for (int index: result.keySet()) {
//            System.out.println(index + ":" + result.get(index));
//        }

        DescriptiveStatistics stats = new DescriptiveStatistics();

// Add the data from the array
        for( int i = 0; i < t.length; i++) {
            stats.addValue(t[i]);
        }

        // Compute some statistics
        double median = stats.getMean();
        System.out.println(new BigDecimal(median).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());


        DoubleArrayList doubleSpeedArraylist = new DoubleArrayList(t);
        System.out.println(new BigDecimal(Descriptive.median(doubleSpeedArraylist)).setScale(0,BigDecimal.ROUND_HALF_UP).intValue());

    }

    public static Map<Integer,Double> smoothFilling(double[] list) {
        Map<Integer,Double> additionalMap = new HashMap<>();
        int lastNotEmptyIndex = -1;
        int firstNotEmptyIndex = -1;
        for (int i=0; i < list.length; i++) {
            if (list[i] != 0) {
                if (lastNotEmptyIndex == -1) {
                    firstNotEmptyIndex = i;
                    lastNotEmptyIndex = i;
                    continue;
                } else {
                    double stepValue = Math.abs((list[i] - list[lastNotEmptyIndex])/(i - lastNotEmptyIndex));
                    for (int j=lastNotEmptyIndex+1; j < i; j++) {
                        list[j] = list[j-1] + stepValue;
                        if (list[i] > list[lastNotEmptyIndex]) {
                            list[j] = list[j-1] + stepValue;
                            additionalMap.put(j, list[j-1] + stepValue);
                        }else {
                            list[j] = list[j-1] - stepValue;
                            additionalMap.put(j, list[j-1] - stepValue);
                        }
                    }
                    lastNotEmptyIndex = i;
                }
            }
        }
        if (firstNotEmptyIndex != -1 && lastNotEmptyIndex != -1 ) {
            for (int i=0; i < firstNotEmptyIndex; i++) {
                list[i] = list[firstNotEmptyIndex];
                additionalMap.put(i, list[firstNotEmptyIndex]);
            }
            for (int i=lastNotEmptyIndex; i < list.length; i++) {
                list[i] = list[lastNotEmptyIndex];
                additionalMap.put(i, list[lastNotEmptyIndex]);
            }
        }
        return additionalMap;
    }
}























