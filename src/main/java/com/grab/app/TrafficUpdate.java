package com.grab.app;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.conveyal.osmlib.OSM;
import com.grab.data.TrafficCondition;
import com.grab.data.VehicleLocation;
import com.grab.engine.traffic.TrafficEngine;
import com.grab.engine.traffic.TrafficEngineExecuter;
import com.grab.external.controlcenter.VehicleTypeFilter;
import com.grab.kinesis.model.Location;
import com.grab.kinesis.processor.LocationTrafficProcessorFactory;
import com.grab.kinesis.util.ConfigurationUtils;
import com.grab.kinesis.util.CredentialUtils;
import com.grab.output.OsrmWriter;
import com.grab.statsd.StatsD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by hubo on 16/3/14.
 */
public class TrafficUpdate {
    private static final Log Logger = LogFactory.getLog(TrafficUpdate.class);
    private static final String CONFIG_FILE_PATH="application.conf";
    public static Properties appProps = new Properties();
    public static OSM osm;
    public static TrafficCondition trafficCondition;
    public static VehicleLocation location;
    public static TrafficEngineExecuter trafficEngineExecuter;
    public static StatsD statsD;
    public static BlockingQueue<Location> locationQueue;
    public static TrafficEngine trafficEngine;
    public static Set<Integer> bikeVehicleIDs = new LinkedHashSet<>();

    public static void main(String[] args) throws Exception {
        // load settings file
        loadSettings(CONFIG_FILE_PATH);

        loadBikeTypeFilter();

        startOSM();

        Integer numberOfWorkerCores = startTrafficEngine();

        startStatD(numberOfWorkerCores);

        startStatMap();

        startKinesis();
    }

    private static void startOSM() {
        Logger.info("======== OSM start ======== ");
        osm = new OSM(null);
        osm.readFromFile(appProps.getProperty("application.osm.dir"));
        Logger.info("======== OSM end ======== ");
    }

    private static Integer startTrafficEngine() {
        Logger.info("======== TRAFFIC ENGINE start ======== ");
        trafficEngine = new TrafficEngine();
        trafficEngine.setStreets(osm);

        trafficEngineExecuter = new TrafficEngineExecuter();

        Integer numberOfWorkerCores;
        try {
            numberOfWorkerCores = Integer.parseInt(appProps.getProperty("application.engine.worker.num"));
        } catch(Exception e) {
            numberOfWorkerCores = Runtime.getRuntime().availableProcessors() / 2;
            Logger.info("Property numberOfWorkerCores not set, defaulting to " + numberOfWorkerCores + " cores.");

        }

        Integer queueLimit = Integer.parseInt(appProps.getProperty("application.engine.queue.limit"));
        locationQueue = new ArrayBlockingQueue<>(queueLimit);
        trafficEngineExecuter.start(numberOfWorkerCores,locationQueue);
        Logger.info("======== TRAFFIC ENGINE end ======== ");
        return numberOfWorkerCores;
    }

    private static void startStatD(Integer numberOfWorkerCores) {
        Logger.info("========StatD start ======== ");
        statsD  = new StatsD();
        statsD.initiate(numberOfWorkerCores);
        Logger.info("========StatD end ======== ");
    }

    private static void startStatMap() {
        trafficCondition = new TrafficCondition();
    }

    private static void startKinesis() throws Exception {
        Logger.info("========Kinesis start ======== ");
        Region region = RegionUtils.getRegion(appProps.getProperty("application.kinesis.region"));
        if (region == null) {
            System.err.println("ap-southeast-1 is not a valid AWS region.");
            System.exit(1);
        }

        String workerId = String.valueOf(UUID.randomUUID());

        AWSCredentialsProvider credentialsProvider = CredentialUtils.getCredentialsProvider();

        KinesisClientLibConfiguration kclConfig =
                new KinesisClientLibConfiguration(ConfigurationUtils.getClientConfigWithUserAgent().getUserAgent(), appProps.getProperty("application.kinesis.stream.name"), credentialsProvider, workerId)
                        .withRegionName(region.getName())
                        .withCommonClientConfig(ConfigurationUtils.getClientConfigWithUserAgent())
                        .withMaxRecords(Integer.parseInt(appProps.getProperty("applicaiton.kinesis.max.records")))
                        .withIdleTimeBetweenReadsInMillis(Long.parseLong(appProps.getProperty("application.kinesis.read.interval")));


        IRecordProcessorFactory recordProcessorFactory = new LocationTrafficProcessorFactory();

        // Create the KCL worker with the location record processor factory
        Worker worker = new Worker(recordProcessorFactory, kclConfig);

        long intervarlMillis = Long.parseLong(appProps.getProperty("application.update.osrm.interval")) * 60 * 1000;//millisecond
        OsrmWriter osrmWriter = new OsrmWriter(System.currentTimeMillis() + intervarlMillis);

        int exitCode = 0;
        try {
            osrmWriter.start();
            worker.run();
        } catch (Throwable t) {
            Logger.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        System.exit(exitCode);
    }

    private static void loadSettings(String configFilePath) {
        if(configFilePath == null)
            configFilePath = "application.conf";
        try {
            FileInputStream in = new FileInputStream(configFilePath);
            appProps.load(in);
            in.close();

            File updatePath = new File(appProps.getProperty("application.update.dir"));
            updatePath.mkdirs();

            File updateAppPath = new File(appProps.getProperty("application.update.app.dir"));
            updateAppPath.mkdirs();

        } catch (IOException e) {
            System.err.println("Unable to load application.conf file.");
            System.exit(1);
        }
    }

    public static void loadBikeTypeFilter(){
        VehicleTypeFilter filter = new VehicleTypeFilter();
        bikeVehicleIDs = filter.getBikeIDs();
    }

}
