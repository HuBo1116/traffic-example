package com.grab.app;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hubo on 16/8/18.
 */
public class MockBooking {
    public static void main(String[] args) throws IOException {
        String startFileName = "mock_booking/mock_booking_start";
        String endFileName = "mock_booking/mock_booking_end";
        String mockBookingName = "mock_booking/mock_booking_result";
        Map<String,String> bookingMap = new HashMap<>();

        File fout = new File(mockBookingName);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        try (BufferedReader br = new BufferedReader(new FileReader(startFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] bookingInfo = line.split("\\|");
                if (bookingInfo.length == 8) {
                    bookingMap.put(bookingInfo[0],line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader(endFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] bookingEndInfo = line.split("\\|");
                if (bookingEndInfo.length == 8) {
                    if (bookingMap.get(bookingEndInfo[0]) != null) {
                        StringBuilder sb = new StringBuilder();
                        String[] bookingStart = bookingMap.get(bookingEndInfo[0]).split("\\|");
                        sb.append(bookingStart[0]).append(",").append(bookingStart[1]).append(",").append(bookingStart[2]).append(",").append(bookingStart[3]).append(",").append(bookingStart[4]).append(",");
                        sb.append(bookingEndInfo[2]).append(",").append(bookingEndInfo[3]).append(",").append(bookingEndInfo[4]);
                        bw.write(sb.toString());
                        bw.newLine();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bw.close();
    }
}
