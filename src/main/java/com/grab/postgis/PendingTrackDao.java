package com.grab.postgis;

import com.grab.app.EnrichMap;
import com.grab.engine.enrich.PendingTrack;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.postgis.Geometry;
import org.postgis.LineString;
import org.postgis.PGgeometry;
import org.postgis.Point;
import org.postgresql.util.PGobject;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by hubo on 16/4/26.
 */
public class PendingTrackDao {
    private static final Log Logger = LogFactory.getLog(PendingTrackDao.class);
    private static final GeometryFactory geoetryFactory =  new GeometryFactory();

    public void save(PendingTrack pendingTrack){
        Connection conn = null;
        try {
            conn = EnrichMap.source.getConnection();
            LineString track = transVividLineStringToPostgisLineString(pendingTrack.getPendingPointsGeom());
            LineString startBarrier = transVividLineStringToPostgisLineString(pendingTrack.getStartCrossing().getTripline().getGeom());
            LineString endBarrier = transVividLineStringToPostgisLineString(pendingTrack.getEndCrossing().getTripline().getGeom());

            String insertSql = "INSERT INTO pending_track (find_time,driver_id,original_track,start_barrier,end_barrier,start_way_id,end_way_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(insertSql);
            Calendar cal = Calendar.getInstance();
            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
            statement.setTimestamp(1,timestamp);
            statement.setInt(2, Integer.parseInt(pendingTrack.getVehicleId()));
            statement.setObject(3, new PGgeometry(track));
            statement.setObject(4, new PGgeometry(startBarrier));
            statement.setObject(5, new PGgeometry(endBarrier));
            statement.setInt(6, (int)pendingTrack.getStartCrossing().getTripline().getWayId());
            statement.setInt(7, (int) pendingTrack.getEndCrossing().getTripline().getWayId());

            int rows = statement.executeUpdate();

            if (rows <= 0) {
                Logger.error("insert postgis failed:"+pendingTrack);
            }

        } catch(SQLException e) {
            Logger.error(e);
        } finally {
            if(conn != null) {
                try {
                    conn.close();
                }catch(SQLException e) {
                    Logger.error(e);
                }
            }
        }
    }

    public List<PendingTrack> findToday(){
        List<PendingTrack> result = new ArrayList<>();
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = EnrichMap.source.getConnection();
            String findSql = "SELECT id, original_track::geometry FROM pending_track WHERE state = 0 and find_time >= ? and find_time <= ?";
            PreparedStatement statement = conn.prepareStatement(findSql);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Timestamp startTime = new Timestamp(cal.getTimeInMillis());
            statement.setTimestamp(1,startTime);
            cal.set(Calendar.HOUR_OF_DAY,24);
            Timestamp endTime = new Timestamp(cal.getTimeInMillis());
            statement.setTimestamp(2,endTime);

            rs = statement.executeQuery();
            while (rs.next()) {
                PendingTrack track = new PendingTrack();
                track.setId(rs.getInt(1));
                PGgeometry geom = (PGgeometry)rs.getObject(2);
                com.vividsolutions.jts.geom.LineString pendingPointsGeom = transPostgisLineStringToVividLineString((LineString)geom.getGeometry());
                track.setPendingPointsGeom(pendingPointsGeom);
                result.add(track);
            }
        } catch(SQLException e) {
            e.printStackTrace();
            Logger.error(e);
        } finally {
            if(rs != null) {
                try {
                    rs.close();
                }catch(SQLException e) {
                    Logger.error(e);
                }
            }
            if(conn != null) {
                try {
                    conn.close();
                }catch(SQLException e) {
                    Logger.error(e);
                }
            }
        }
        return result;
    }

    public void updateOptimizedTrack(PendingTrack pendingTrack){
        Connection conn = null;
        try {
            conn = EnrichMap.source.getConnection();
            LineString optimizedTrack = transVividLineStringToPostgisLineString(pendingTrack.getOptimizedPointsGeom());

            String updateSql = "UPDATE pending_track SET optimized_track = ? WHERE id = ?";
            PreparedStatement statement = conn.prepareStatement(updateSql);

            statement.setObject(1, new PGgeometry(optimizedTrack));
            statement.setObject(2, pendingTrack.getId());

            int rows = statement.executeUpdate();

            if (rows <= 0) {
                Logger.error("update optimized track failed:"+pendingTrack);
            }

        } catch(SQLException e) {
            e.printStackTrace();
            Logger.error(e);
        } finally {
            if(conn != null) {
                try {
                    conn.close();
                }catch(SQLException e) {
                    Logger.error(e);
                }
            }
        }
    }


    public static org.postgis.LineString transVividLineStringToPostgisLineString(com.vividsolutions.jts.geom.LineString vividLineString){
        Coordinate[] coordinates = vividLineString.getCoordinates();
        Point[] points = new Point[coordinates.length];
        for(int i=0; i< coordinates.length; i++){
            Point point = new Point(coordinates[i].x,coordinates[i].y);
            points[i] = point;
        }
        return new org.postgis.LineString(points);
    }

    public static com.vividsolutions.jts.geom.LineString transPostgisLineStringToVividLineString(org.postgis.LineString postgisLineString){
        Point[] points = postgisLineString.getPoints();
        Coordinate[] coordinates = new Coordinate[points.length];
        for(int i = 0; i < points.length; i++){
            Coordinate coordinate = new Coordinate(points[i].x,points[i].y);
            coordinates[i] = coordinate;
        }
        return geoetryFactory.createLineString(coordinates);
    }

    public static void main(String[] args){
        EnrichMap.loadSettings("application.conf");
        EnrichMap.startPostgis();
        PendingTrackDao dao = new PendingTrackDao();
        List<PendingTrack> list = dao.findToday();
        for(PendingTrack track: list) {
            System.out.println(track.getPendingPointsGeom());
        }
    }
}
