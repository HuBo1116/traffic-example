package com.grab.external.google;

import com.grab.app.EnrichMap;
import com.grab.engine.enrich.PendingTrack;
import com.grab.engine.geom.GPSPoint;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hubo on 16/5/9.Ø
 */
public class SnapToRoad {
    private static String GOOGLE_SNAP_TO_ROAD_API = EnrichMap.appProps.getProperty("application.google.snap2road.api");
    private static String GOOGLE_SNAP_TO_ROAD_KEY = EnrichMap.appProps.getProperty("application.google.snap2road.key");
    public static int GOOGLE_SNAP_TO_ROAD_LIMIT = Integer.parseInt(EnrichMap.appProps.getProperty("application.google.snap2road.limit"));

    public static List<SnapToRoadLocation> getGoogleSnappedTrack(PendingTrack pendingTrack) throws IOException {
        String path = assemblePath(pendingTrack);
        if ("".equals(path)){
            return new ArrayList<>();
        }

        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 3000);
        HttpClient client = new DefaultHttpClient(httpParams);

        StringBuilder url = new StringBuilder();
        url.append(GOOGLE_SNAP_TO_ROAD_API).append(path);
        url.append("&key=").append(GOOGLE_SNAP_TO_ROAD_KEY);
        System.out.println(url.toString());
        HttpGet request = new HttpGet(url.toString());

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();
        return SnapToRoadLocation.fromJsonAsBytes(EntityUtils.toByteArray(entity));
    }

    private static String assemblePath(PendingTrack track) throws UnsupportedEncodingException {
        if (track.getPendingPointsGeom() == null || track.getPendingPointsGeom().getCoordinates().length <= 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for(Coordinate coordinate: track.getPendingPointsGeom().getCoordinates()){
            sb.append(coordinate.y).append(",").append(coordinate.x).append("|");
        }
        return URLEncoder.encode(sb.toString().substring(0, sb.toString().length() - 1), "UTF-8");
    }
}

