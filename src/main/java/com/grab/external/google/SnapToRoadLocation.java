package com.grab.external.google;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by hubo on 16/5/9.
 */
public class SnapToRoadLocation {
    private double latitude;
    private double longitude;

    public final static ObjectMapper JSON = new ObjectMapper();
    static {
        JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static List<SnapToRoadLocation> fromJsonAsBytes(byte[] bytes) {
        List<SnapToRoadLocation> locations = new ArrayList<>();
        try {
            JsonNode response = JSON.readValue(bytes, JsonNode.class);
            if (response.get("snappedPoints")!=null && response.get("snappedPoints").size() > 0){
                for(int i=0; i<response.get("snappedPoints").size();i++){
                    SnapToRoadLocation location = JSON.readValue(response.get("snappedPoints").get(i).get("location").toString(),SnapToRoadLocation.class);
                    locations.add(location);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
