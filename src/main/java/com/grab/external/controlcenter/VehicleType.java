package com.grab.external.controlcenter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hubo on 16/3/24.
 */
public class VehicleType {
    private long id;
    private String name;
    private String serviceType;
    private String currencySymbol;
    private String currencyCode;
    private boolean delivery;
    private String cityCode;
    private String peakHourStartTime;
    private String peakhourendtime;
    private String peakHourDays;

    public final static ObjectMapper JSON = new ObjectMapper();
    static {
        JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static List<VehicleType> fromJsonAsBytes(byte[] bytes) {
        try {
            return JSON.readValue(bytes, new TypeReference<List<VehicleType>>(){});
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getPeakHourStartTime() {
        return peakHourStartTime;
    }

    public void setPeakHourStartTime(String peakHourStartTime) {
        this.peakHourStartTime = peakHourStartTime;
    }

    public String getPeakhourendtime() {
        return peakhourendtime;
    }

    public void setPeakhourendtime(String peakhourendtime) {
        this.peakhourendtime = peakhourendtime;
    }

    public String getPeakHourDays() {
        return peakHourDays;
    }

    public void setPeakHourDays(String peakHourDays) {
        this.peakHourDays = peakHourDays;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

}
