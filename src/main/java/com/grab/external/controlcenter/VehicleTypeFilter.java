package com.grab.external.controlcenter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.grab.app.TrafficUpdate;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


/**
 * Created by hubo on 16/3/24.
 */
public class VehicleTypeFilter {
    private static String VEHICLE_API = TrafficUpdate.appProps.getProperty("application.cc.vehicle.api");
    private static String PARAM = "ALL";
    private static String BIKE_TYPE = "BIKE";

    public Set<Integer> getBikeIDs(){
        Set<Integer> bikeIDs = new LinkedHashSet<>();
        List<VehicleType> list;
        try {
            list = getAll();
        } catch (IOException e) {
            try {
                list = readVehicleDataFromLocalFile();
            } catch (IOException e1) {
                list = new ArrayList<>();
            }
        } catch (NoSuchAlgorithmException e) {
            list = new ArrayList<>();
        }
        for(VehicleType vehicleType:list){
            if (vehicleType.getServiceType().equals(BIKE_TYPE)){
                bikeIDs.add(Integer.parseInt(vehicleType.getId()+""));
            }
        }
        return bikeIDs;
    }

    private List<VehicleType> getAll() throws IOException, NoSuchAlgorithmException {
        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 3000);
        HttpClient client = new DefaultHttpClient(httpParams);
        if (VEHICLE_API == null) {
            VEHICLE_API = "https://control-centre-int.myteksi.net/api/system/v1/vehicle_types?cityIDs=ALL";
        }
        HttpGet request = new HttpGet(VEHICLE_API);
        request.addHeader("Signature", generateSignature(PARAM));
        request.addHeader("Authorization","GRAB_ROAD");

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();
        return VehicleType.fromJsonAsBytes(EntityUtils.toByteArray(entity));
    }
    private String generateSignature(String param) throws NoSuchAlgorithmException {
        String salt = TrafficUpdate.appProps.getProperty("application.cc.vehicle.salt");
        if (salt == null) {
            salt = "w1wgVo9qmJQAcRcdn9z2D7jfpOd8NCMlCP6CHfLGnBCxPziqv4yUA04Hckh+aKHnyJXGG1tR7Of2LYdV7oqZtw==";
        }
        param += ","+salt;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(param.getBytes());
        return Base64.encodeBase64String(digest);
    }

    public static void main(String[] args){
        VehicleTypeFilter filter = new VehicleTypeFilter();
        Map<String,Set<String>> map = filter.getAllCityCode();
        for (String currency: map.keySet()){
            StringBuilder sb = new StringBuilder();
            sb.append("currency-").append(currency).append(":");
            Set<String> cityCode = map.get(currency);
            for (String code:cityCode) {
                sb.append(code).append(",");
            }
            System.out.println(sb.toString());
        }
    }

    public List<VehicleType> readVehicleDataFromLocalFile() throws IOException {
        return VehicleType.JSON.readValue(new File("vehicle.json"), new TypeReference<List<VehicleType>>(){});
    }



    public Map<String,Set<String>> getAllCityCode(){
        Map<String,Set<String>> map = new HashMap<>();
        List<VehicleType> list;
        try {
            list = readVehicleDataFromLocalFile();
        } catch (IOException e) {
           list = new ArrayList<>();
        }
        for(VehicleType vehicleType:list){
            Set<String> cityCodes = map.get(vehicleType.getCurrencyCode());
            if (cityCodes == null){
                cityCodes = new HashSet<>();
                cityCodes.add(vehicleType.getCityCode());
                map.put(vehicleType.getCurrencyCode(),cityCodes);
            }else{
                cityCodes.add(vehicleType.getCityCode());
            }
        }
        return map;
    }
}
